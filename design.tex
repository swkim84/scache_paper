\section{Which Type of Write is Critical?}\label{sec:DesignIssue}

Our goal is to effectively use NVWC by carefully distilling the critical writes out of all the requested writes so that NVWC admits only writes worth improving application performance. In the following subsections, we first describe the rational behind our classification scheme that distinguishes criticality of each write in terms of application performance. We then explain challenges for accurately classifying the critical writes induced by complex dependencies in applications and system software.  

\subsection{Request-Oriented Write Classification}

The primary role of a storage application is to provide a specific storage service in response to an external \textit{request}, like a request to a database server for processing an e-commerce transaction. Consequently, the performance of request processing determines the level of application performance that application users perceive. Therefore, it is more important to consider which type of write delays the progress of request processing than the characteristics of write I/O itself such as file or block type and access pattern, for distinguishing the criticality of each write. 

Synchronous writes, hence, can be a good candidate for the type of critical write. Traditionally, write I/O is broadly classified into two categories in the system's viewpoint: asynchronous and synchronous. When a process~\footnote{We use process to refer any kind of execution context in this paper.} issues an asynchronous write, it can immediately continue other processing jobs without waiting for the completion of the write. A synchronous write, on the other hand, is awaited by a requesting process until the write completes. Due to this difference, prioritizing synchronous writes over asynchronous ones is known as a reasonable method to reduce system-wide I/O wait time~\cite{Ganger1993}, and hence it is adopted in commodity OS~\cite{LinuxWriteSync}. 

\begin{table}
\begin{center}
\begin{footnotesize}
\begin{tabular}{|c||c|r|}
\hline
\textbf{Write Type} & \textbf{Process} & \textbf{Ratio (\%)} \\
\hline \hline
\multirow{5}{*}{\textbf{Sync.}}
        & backends &  44.312 \\
    \cline{2-3}
            & checkpointer & 34.664 \\
        \cline{2-3}
        & log writer & 0.368 \\
    \cline{2-3}
            & jbd2 & 0.094 \\
        \cline{2-3}
        & etc & 0.007 \\
    \hline

    \multirow{2}{*}{\textbf{Async.} }
            & kworker & 20.554 \\
        \cline{2-3}
        & etc & 0.002 \\
    \cline{2-3}
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\caption{\textbf{Ratio of writes issued per process.} \textit{We ran the TPC-C benchmark and recorded the amount and the type of writes per process at the block-level. Backends, checkpointer, and log writer are user-level processes belonging to PostgreSQL; see Section~\ref{sec:Applications} for the details. Jbd2 and kworker are kernel-level processes that conduct journaling I/Os for ensuring consistency of ext4 file system and cleaning of OS buffer cache, respectively.}}
\label{tab:tpcc-io-breakdown}
\end{table}

However, not all the synchronous writes are truly synchronous from the perspective of \textit{request execution}. Typical storage applications delegate a large amount of synchronous writes to a set of background processes as a way of carrying out internal activities. For instance, storage applications like RDBMSes~\cite{Gray1992,Malviya2014}, NoSQL stores~\cite{Chang2006,DeCandia2007,RedisPersistence}), and file systems~\cite{Prabhakaran2005} adopt variants of logging technique that accompanies only a small amount of (mostly sequential) synchronous writes during request processing while conducting a burst of (mostly random) synchronous writes in background. This is an intrinsic design to make costly synchronous writes off the critical path of request execution as much as possible for achieving high degree of application performance without loss of durability. 

To validate such behaviors, we ran the TPC-C benchmark and recorded the type and the amount of write issued per process; see Section~\ref{sec:Evaluation} for the detailed configuration. As shown in Table~\ref{tab:tpcc-io-breakdown}, about 80\% of the writes are synchronous, and most of the synchronous writes are performed by \textit{backends} and \textit{checkpointer}. In PostgreSQL, backend is a dedicated process for handling requests from a client while checkpointer periodically issues a burst of synchronous writes to clean the buffer pool dirtied by backend; see Section~\ref{sec:Applications} for more details on PostreSQL. Likewise, in kernel-level, \textit{jbd2} also issues synchronous writes (though small amount in this case) for committing and checkpointing file system transactions. Basically, the synchronous writes requested by the processes other than the backends are irrelevant to processing database queries from the clients. Furthermore, according to our analysis result (Table~\ref{tab:tpcc-cached-data-breakdown}), asynchronous writes occasionally block backends because of complex runtime dependencies; we explain the dependency problems in the following subsection. The conventional synchrony-based write classification, therefore, is not adequate for classifying critical writes in terms of application performance.

We introduce \textit{request-oriented write classification} that classifies only a write awaited to be completed in the context of request execution as critical irrespective of whether it was issued synchronously or not. Based on this concept, we classify all the writes into two categories: \textit{critical write} and \textit{non-critical write}. Then, only the critical writes are cached into NVWC so that the request can be handled quickly while a large amount of non-critical writes are routed to backing storage directly. To the best of our knowledge, this is the first attempt to classify write I/Os based on the context of request execution for improving application performance.

The main benefits of the proposed classification are threefold. First, unnecessary stalled writes in a critical path can be avoided as much as possible by directly routing a burst of non-critical writes to backing storage. Second, severe queue congestion in the request queues of NVWC incurred by bandwidth-hungry background I/O jobs can be largely relaxed, thereby effectively reducing queueing delay of critical writes. Third, device-specific reliability and performance issues can be eased. For example, in the cases of flash- and PCM-based NVWC, the lifetime of the NVWC can be extended by reducing the number of cached writes without hurting application performance. 

\subsection{Dependency-Induced Critical Write}

In most storage applications, one or several processes are dedicated to handle requests while other processes carry out internal activities in background. Synchronous writes issued by such processes are definitely critical, because if these processes are blocked for waiting write I/Os, the delays will directly affect application performance; hence, we refer to this type of process as a \textit{critical process} throughout this paper. Therefore, a straightforward criticality-aware admission policy is to cache synchronous writes requested only by critical processes to NVWC. The simple admission policy alone, however, is insufficient for identifying all the critical writes because of runtime dependencies in an application and system software. 

There are two types of dependencies associated with write I/Os: process dependency and I/O dependency. Process dependency occurs when two processes interact with each other via process synchronization primitives such as a lock, barrier, and condition variable. The process dependency between a critical process and a non-critical process complicates the accurate detection of critical writes because the non-critical process may issue synchronous writes within a critical section making the critical process indirectly waiting for that writes to be completed. Unlike process dependency, I/O dependency occurs between a critical process and an on-going I/O operation (i.e., non-critical write). Basically, I/O dependency is generated when a critical process needs to wait for the completion of an on-going write I/O, which is already issued by a non-critical process, in order to ensure consistency and/or durability. Hence, if we fail to detect such dependency-induced critical writes, the progress of a critical process will be significantly delayed. 

%Unlike process dependency, I/O dependency occurs between a critical process and an on-going I/O operation (i.e., non-critical write). Basically, I/O dependency is generated when a process attempting to modify a buffer page on which a write I/O is in progress. In this case, the process needs to wait for the completion of the on-going write I/O when the buffer contains critical data in terms of file system-level consistency. Hence, if a critical process needs to modify a buffer page on which a non-critical process already has issued write I/O, the critical process will be blocked until the non-critical write completes. Hence, if we fail to detect such I/O dependency-induced critical writes, the progress of a critical process will be significantly delayed. \sw{Example of dependency to writes issued asynchronously by kernel thread for cleaning dirtied data and metadata of OS buffer cache.}

\begin{figure}
\centering
\epsfig{file=figures/design-analysis-dependency.eps, width=3in}
\caption{\textbf{Impact of dependencies on application performance.} \textit{We measured system latencies for backends in PostgreSQL during the execution of the TPC-C benchmark with 4GB ramdisk NVWC. We used two NVWC admission policies: CP and ALL. CP caches synchronous writes requested by critical processes while ALL caches all the writes without any discretion. We omit network-related latencies for brevity.}}
\label{fig:design-analysis-dependency}
\end{figure}

In order to quantify the significance of the dependency problems, we measured the system latencies of critical processes (i.e., backends) using \textit{LatencyTOP}~\cite{Edge2008} during the execution of the TPC-C benchmark with 4GB ramdisk used as NVWC; see Section~\ref{sec:Evaluation} for the detailed configuration. Figure~\ref{fig:design-analysis-dependency} shows the impact of complex dependencies on the TPC-C throughput; CP caches only synchronous writes requested by critical processes while ALL caches all the writes without any differentiation. As we expected, CP mostly eliminates the latency results from synchronous writes (\texttt{wait\_on\_page\_writeback}). However, CP still suffers from excessive latencies mainly caused by process dependency (\texttt{mutex\_lock}) and I/O dependency (\texttt{sleep\_on\_shadow\_bh}). Note that the I/O dependency occurs because a critical process attempts to update a buffer page that is under writing back as a part of committing file system transaction to the journal area of ext4 file system. On the other hand, ALL eliminates most of the latencies because ALL regards all the writes as critical and the configured NVWC capacity was enough not to stall any writes. As a consequence, ALL could achieve about 48\% better performance than CP.

\begin{table}
\begin{center}
\begin{footnotesize}
\begin{tabular}{|c||c|r|r|}
\hline
\textbf{Type} & \textbf{Synchronization Method} & \textbf{Avg (ms)} & \textbf{Max (ms)} \\
\hline \hline
\multirow{5}{*}{\textbf{Process}}
        & \texttt{down\_read} & 952.82 & 4251.54 \\
    \cline{2-4}
            & \texttt{wait\_transaction\_locked} & 447.29 & 3295.74 \\
        \cline{2-4}
        & \texttt{mutex\_lock} & 141.26 & 6516.73 \\
    \cline{2-4}
            & \texttt{jbd2\_log\_wait\_commit} & 37.86 & 462.05 \\
    \hline

    \multirow{4}{*}{\textbf{I/O} }
            & \texttt{lock\_buffer} & 749.93 & 2222.57 \\
        \cline{2-4}
        & \texttt{sleep\_on\_shadow\_bh} & 235.34 & 4364.33 \\
    \cline{2-4}
            & \texttt{lock\_page} & 0.09 & 10.72 \\
        \cline{2-4}
        & \texttt{wait\_on\_page\_writeback} & 0.04 & 30.93 \\
    \cline{2-4}
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\caption{\textbf{Sources of dependencies.} \textit{We report the average and maximum latencies of PostgreSQL backend processes in the case of CP.}}
\label{tab:tpcc-cp-latency}
\end{table}

In addition, there are many other sources of excessive latencies in terms of the average and worst case as shown in Table~\ref{tab:tpcc-cp-latency}. Read/write semaphore-induced latency represented as \texttt{down\_read()} is about one second and several seconds in the average and worst case, respectively. The file system journaling-related synchronizations for ensuring file system consistency also incur high latencies. Specifically, \texttt{wait\_transaction\_locked()} is called to synchronize with all the processes updating the current transaction to complete their execution while \texttt{jbd2\_log\_wait\_commit()} is called to wait for the journaling daemon to complete the commit procedure of a requested file system transaction. The synchronization methods that induce I/O dependency such as \texttt{lock\_buffer()} make the progress of critical processes to be excessively delayed up to several seconds. Though some of the synchronization methods is insignificant in total, we should handle all of them for providing consistent responses to users.

Since synchronization is frequently used to ensure correct execution of concurrent processes and I/Os, the writes issued by non-critical processes can be the performance bottleneck of request execution. This problem can be more severe when a large number of concurrent non-critical writes are contending for the available bandwidth of backing storage because the non-critical writes a critical process depends on to make progress takes longer time to be completed than when the disk is not congested. Accordingly, it is crucial to carefully consider process and I/O dependencies associated with critical processes for accurately identifying all critical writes.

\section{Critical Write Detection}\label{sec:DesignSolution}

This section presents how the proposed scheme identifies all critical writes in the presence of complex runtime dependencies. We first illustrate our approach for distinguishing critical processes at kernel-level. Then, we explain how to resolve dynamically generated dependencies associated with critical processes.

\subsection{Critical Process Identification}

To detect critical writes, the first thing we need to address is how can we accurately detect which processes are critical among various processes in the system. There are two possible approaches for identifying critical processes: an OS-level only approach and an application-guided approach. 

An OS-level only approach is based on monitoring and tracking inter-process communication (IPC) at the kernel-level to infer a set of important processes. For example, similar to the previous approaches~\cite{Zheng2004,Zheng2010}, we can track processes involved in handling requests by monitoring system calls related to socket communication. The main strength of this approach is it does not require application modifications. However, it requires lots of instrumentation to various in-kernel methods in order to generally cover all kinds of IPC. In addition, it also requires some heuristics (e.g., feedback-based confidence evaluation~\cite{Zheng2004}) to reduce the possibility of misidentification.

On the other hand, application-guided approach can precisely identify a set of critical processes without complex instrumentations and heuristics at kernel-level, by directly exploiting application-level semantic. For instance, an application can simply notify critical processes involved in the course of request execution to the kernel through a system call interface. This approach is based on the fact that an application developer knows the best about the application semantic. The main weakness of application-guided approach is that it requires application modification to notify the information on critical processes to the kernel. 

In this work, we adopt application-guided approach as our critical process identification for the following reasons. First, it can precisely identify all the critical processes in the system with low engineering cost, and hence less error-prone in the kernel-level. Second, it is easy to implement and portable since the information on critical processes provided by an application is a disclosing hint~\cite{Patterson1995} that does not require any specific knowledge about underlying systems the application running on. Third, it requires adding only few new lines of code to an application in practice because typical storage applications (e.g., MySQL~\cite{MySQL}) already distinguishes foreground process (i.e., critical process) from background process (i.e., non-critical process); see Section~\ref{sec:Applications} for our application studies. 

Based on the hint on critical processes, synchronous writes requested by critical processes are cached in NVWC. Since the application hint is solely used for deciding admission of each write to NVWC, a wrong hint does not affect to the correct execution of the application. However, hint abuse by a malicious or a thoughtless application may compromise performance isolation among multiple applications (or multiple tenants) sharing NVWC. Addressing the fairness issue results from sharing NVWC among multiple applications is out of scope of this paper, hence we remain the detailed investigation on this issue as our future work.

\subsection{Dependency Handling Techniques}

As we discussed in Section~\ref{sec:DesignIssue}, dependencies between a critical process and either a non-critical process (i.e., process dependency) or on-going non-critical write (i.e., I/O dependency) would significantly delay the request execution. In order to deal with the dependency problem, we introduce \textit{criticality inheritance protocols} to handle the process and I/O dependencies. In addition, we devise a technique called \textit{blocking status tracking} for handling a case where a target process for criticality inheritance is already blocked. We now describe each.

\subsubsection{Criticality Inheritance Protocols}

\textbf{Process criticality inheritance.} Handling process dependency has been well studied in the context of process scheduling because process dependency may cause priority inversion problem~\cite{Lampson1980}. Priority inheritance~\cite{Sha1990} is the well-known solution for resolving priority inversion problem caused by process dependency. The basic idea behind priority inheritance is that when a process blocks higher priority processes, it executes its critical section at the highest priority level of all the processes it blocks. Later, automatic detection of IPC-induced process dependencies also has been investigated for effective process scheduling~\cite{Zheng2004}. The basic mechanism of automatic detection is tracking system call history to determine possible resource dependencies among processes.

Inspired by the previous work, we introduce \textit{process criticality inheritance} for resolving process dependency. The procedure of conducting process criticality inheritance is similar to priority inheritance in that a non-critical process inherits criticality when it blocks critical processes until it finishes its execution within the synchronized region. The main difference between process criticality inheritance and priority inheritance is that process criticality inheritance is used for identifying process dependency-induced critical writes that block the execution of a critical process.

\begin{figure}
\centering
\subfigure[Process criticality inheritance\label{fig:design-criticality-inheritance-process}]{\epsfig{file=figures/design-criticality-inheritance-process.eps, width=3in}}
\subfigure[I/O criticality inheritance\label{fig:design-criticality-inheritance-io}]{\epsfig{file=figures/design-criticality-inheritance-io.eps, width=3in}}
\caption{\textbf{Criticality inheritance protocols.}}
\label{fig:design-criticality-inheritance}
\end{figure}

Figure~\ref{fig:design-criticality-inheritance-process} illustrates an example of process criticality inheritance and its relationship with the proposed admission policy. Critical process P1 attempts to acquire a lock to enter a critical section during the request execution (1). Since the lock already held by the process P2, P2 inherits criticality from P1 (2). Then, the synchronous write issued by P2 is directed to the NVWC for the low latency write within the critical path. P2 wakes up P1 when its execution within the critical section has been completed (3), and P1 does the rest of its job for request execution including synchronous writes. In this example, only the three writes are awaited to be completed within the critical path, and hence only those writes are worth caching to improve application performance.

\textbf{I/O criticality inheritance.} Handling I/O dependency is more complicated than that of process dependency since inheriting criticality to the on-going write to a backing storage requires dynamic rerouting to a NVWC. To reroute the on-going write without compromising correctness, we need to reissue the write to the NVWC and invalidate side effects (e.g., duplicated I/O completion) from the on-going write to the disk. 

One of possible solutions for invalidating the side effects of rerouting is locating and discarding the on-going write to disk. In practice, however, locating and discarding a specific on-going write needs significant engineering efforts due to multiple queueing stages in I/O stack. In Linux, for example, an I/O can be staged in an OS request queue managed by I/O scheduler in order to merge and reorder incoming I/Os based on a configured policy. In addition, an I/O also can be staged in a storage device queue managed by the device firmware for better utilizing its internal architecture. Hence, the procedure of locating and discarding requires lots of modifications to various in-kernel components including I/O scheduler and device driver.

In order to rapidly resolve I/O dependency while maintaining the low engineering cost, we devise immediate reissuing and lazy discarding as a rerouting technique for I/O criticality inheritance. Basically, our rerouting scheme maintains a list of write submitted to a disk. Then, when I/O dependency occurs, we reissue the write, which prolongs the progress of a critical process, to a NVWC and tag the corresponding write in the list as invalid. Finally, we discard the original write submitted to the disk upon completion by inspecting the tag. Since the discarding is done upon completion, the proposed technique lowers engineering cost without compromising the correctness.

Figure~\ref{fig:design-criticality-inheritance-io} shows an example of I/O criticality inheritance and its relationship to the proposed admission policy. The process P1 needs to wait for the completion of the write B2, which is already issued by P2 as a non-critical write to backing storage, during the course of request execution (1). To inherit criticality to B2, B2 is rerouted to NVWC by P1 (2). The event of I/O completion on the rerouted B2 wakes up P1 (3), and P1 does the residual work for processing the request. Later, the event of the I/O completion on the original write on B2 is discarded to suppress the duplicated notification of I/O completion to upper layers such as file system. In this example, B2 is the I/O dependency-induced critical write since it indirectly delays the request execution due to synchronization.

The main drawback of the proposed technique for I/O criticality inheritance is that it cannot eliminate unnecessary write traffic to a disk since it does not proactively discard on-going writes to the disk. However, the performance penalty of a duplicated write to the disk would be small since the duplicated write to a specific block is highly likely to be processed as a single request merged with other adjacent blocks by I/O scheduler in common cases. In some cases, discarding several writes included in a single request results in splitting the request into multiple requests, thereby decreasing the efficiency of I/O processing at the disk.

\subsubsection{Blocking Status Tracking}

Though criticality inheritance protocols can handle the dependency caused by currently running processes, they cannot handle the case where a target process is already blocked for synchronizing with another process or I/O. For example, a non-critical process can sleep waiting for another non-critical process to exit from a synchronized region, while holding a lock for another synchronized region. In addition, a non-critical process can also sleep waiting for a write issued to a disk to be completed while holding a lock for a synchronized region. In these scenarios, the progress of the critical process is delayed by the amount of time required to complete the disk write even if the non-critical process inherits criticality. Moreover, cascading process dependencies that may involve an already blocked process make accurate identification of all the critical writes more difficult.

%This problem will be more serious if the disk is highly congested due to the intensive non-critical writes result from an application's background activity (e.g., checkpointing).

%We devise an additional technique called blocking status tracking to complement criticality inheritance protocols. Blocking status tracking is based on the reason (e.g., process dependency) and related object (e.g., process descriptor) that are recorded to a process descriptor right before a process is blocked. When a critical process is about to be blocked by a non-critical process, the critical process follows the process criticality inheritance protocol first, and then it inspects the blocking status of the non-critical process. If the non-critical process is sleeping already due to the dependency to the other process or I/O, the critical process handles that dependency instead of the sleeping non-critical process using the recorded information. Blocking status tracking is recursively conducted if the critical process detects cascading process dependencies to multiple processes. The process who recorded its blocking status clears that information when rescheduled.

\begin{figure}
\centering
\epsfig{file=figures/design-blocking-status-tracking.eps, width=3in}
\caption{\textbf{Blocking status tracking.} \textit{BS denotes the blocking status of each process.}}
\label{fig:design-blocking-status-tracking}
\end{figure}

We devise an additional technique called blocking status tracking to complement criticality inheritance protocols. Figure~\ref{fig:design-blocking-status-tracking} demonstrates an example to describe how blocking status tracking works. The process P3 issues synchronous writes and records its blocking status right before it goes to sleep. The process P2 also records its blocking status because it needs to synchronize with the progress of P3. Later, the critical process P1 attempts to acquire the lock that is held by P2. Then, P2 inherits criticality from P1, and P1 checks P2's blocking status to see if whther P2 is already blocked or not. Since P2 is currently blocked and depends on the progress of P3, P3 also inherits criticality and P1 again checks P3's blocking status. Since P3 currently blocks on the on-going write B1, P1 initiates rerouting of B1 to NVWC, and sleeps until the lock has been released by P2. I/O completion handler wakes up P3 when the rerouted B1 has been completed, and P3 clears its blocking status and wakes up P2 after doing some residual work. P2, in turn, clears its blocking status upon awaking, and wakes up P1 after completing its execution in the critical section. Finally, P1 enters the critical section and conducts the rest of the requested job.

By using criticality inheritance protocols in combination with blocking status tracking, all the dependency-induced critical writes can be effectively handled so that a critical process can proceed quickly without waiting for slow disk I/O latencies.

% Inevitable interaction between database and file system~\cite{Zheng2014}. Nearly all modern databases run through the file system. Of the major commercial OLTP vendors, Oracle has removed support for raw storage devices [33], IBM has deprecated it for DB2 [23], and Microsoft strongly discourages raw partitioning for SQL Server [26].
%What makes resolving these dynamic dependencies harder is that these dependencies can occur in a cascading manner. For example, a critical process needs wait for a non-critical process that holds a lock. But, if the non-critical process tries to update a buffer page which is in part of a committing transaction. Then, the non-critical process is also blocked until the committing transaction completes. Identifying a chain of such dependencies and resolving them is challenging because when a critical process is blocked, the non-critical process is also scheduled out because of its I/O dependency. 

