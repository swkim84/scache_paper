\begin{table}
\begin{center}
\begin{footnotesize}
\begin{tabular}{|c||c|c|c|}
\hline
\textbf{Workload} & \textbf{Brief} & \textbf{Usage} & \textbf{Request} \\
    \textbf{Type} & \textbf{Description} & \textbf{Example} & \textbf{Dist.} \\
\hline \hline
\textbf{A} & Update heavy & Session store &  Zipfian \\
\hline
\textbf{B} & Read mostly & Photo tagging &  Zipfian \\
\hline
\textbf{D} & Read latest & User status updates &  Latest \\
\hline
\textbf{F} & Read-modify-write & User database &  Zipfian \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\caption{\textbf{Description of YCSB workloads.} \textit{The ratios of operations in each workload are as follows: Workload A (read:update = 0.5:0.5), B (read:update = 0.95:0.05), D (read:insert = 0.95:0.05), F (read:read-modify-write = 0.5:0.5). We excluded Workload C and E since they are read-only and scan-mostly workload, respectively.}}
\label{tab:ycsb-workloads}
\end{table}

\section{Evaluation}\label{sec:Evaluation}

This section presents evaluation results based on the prototype implementation and the modified applications using realistic workloads. We first detail the experimental environment. Then, we show the experimental results for both PostgreSQL and Redis to validate the effectiveness of the proposed scheme. 

\subsection{Experimental Setup}

Our prototype was installed on Dell PowerEdge R420, equipped with two quad-core Intel Xeon E5-2407 2.4GHz processors and 16GB RAM; CPU clock frequency is set to the highest level for stable performance measurement. The storage system is comprised of three 500GB 10K RPM WD VelociRaptor HDDs, one of which is dedicated to OS and the others are used as backing storage of NVWC. We used Ubuntu 14.04 with the modified Linux kernel version 3.12 as an OS. For all the experiments, ext4 is used as a file system mounted with the default options.

For NVWC devices, we allocated 4GB region of 16GB RAM to ramdisk for an NVWC device having high performance, but low capacity such as NV-DRAM and STT-MRAM. To correctly emulate NV-DRAM in the existence of CPU writeback caches, we used non-temporal memory copy described in \cite{Chen2014} when the data in ramdisk is written. In addition, we used a 256GB Samsung 840 Pro MLC SSD as an NVWC device that has high capacity, but relatively low performance and limited write endurance such as flash memory and PCM. For the stable measurement, we discard all the blocks in SSD and give enough idle time to erase invalid blocks before starting the experiments with SSD. Write caching to on-board cache of the storage devices, including the HDDs and the SSD, was turned off to eliminate performance variations caused by internal buffering.

\begin{figure}
\centering
\epsfig{file=figures/eval-performance-postgres-ramdisk-capacity.eps, width=3in}
\caption{\textbf{PostgreSQL performance with ramdisk.} \textit{}}
\label{fig:eval-performance-postgres-ramdisk}
\end{figure}

We used TPC-C~\cite{TPCC} and YCSB~\cite{Cooper2010} as the realistic workloads for PostgreSQL and Redis, respectively:

\begin{itemize}
	\item \textbf{TPC-C} is a standard online transaction processing (OLTP) system benchmark simulating an order-entry environment. We used TPC-C scale factor ten, which corresponds to about 1GB of initial database, to make database fitting into main memory so that performance impact of read I/Os can be excluded. We simulated 24 clients for 30 minutes in all runs, otherwise noted. We report the number of New-Order transactions executed per minute (i.e., tpmC) as the performance metric for TPC-C. 
	\item \textbf{YCSB} is an industry-standard performance benchmark for NoSQL databases. We used a set of workloads pre-configured in the benchmark suite; Table~\ref{tab:ycsb-workloads} summarizes the evaluated workloads. The data set was composed of 0.5 million objects each of which is 1KB in size. We simulated 40 clients to generate ten millions of operations in total, and we report operations per second (i.e., ops/s) as the performance metric for YCSB. 
\end{itemize}

For both benchmarks, the clients was run on a separate machine with a system configuration identical to that of the server connected directly via 1Gbps Ethernet port. In addition, we report performance (e.g., tpmC) per cached block for comparing caching efficiency. 

We used two criticality-oblivious admission policies: ALL and SYNC. ALL, which is the FlashCache default, caches all the incoming writes while SYNC caches only the synchronous writes. In addition, we used three criticality-aware admission policies: CP, CP+PCI, and CP+PCI+IOCI. CP caches only synchronous writes requested by critical processes. CP+PCI caches process dependency-induced critical writes in addition to the critical writes detected by CP. CP+PCI+IOCI caches I/O dependency-induced critical writes in addition to the critical writes detected by CP+PCI.

\subsection{PostgreSQL with TPC-C}

PostgreSQL was configured to have 512MB buffer pool and the number of log files triggering background checkpointing (i.e., \textit{checkpoint\_segments}) was set to 16 (i.e., 256MB). The database files and log files are located on different HDD according to the recommendation in the official document~\cite{PostgresStorageSetup}. We warmed up OS buffer cache before starting the benchmark. As a practical alternative to our approach, we used an additional policy denoted as WAL that caches only writes related to log files by solely using NVWC to absorb all the  write traffic to the log disk.

Figure~\ref{fig:eval-performance-postgres-ramdisk} shows the TPC-C throughput averaged over three runs with varying capacity of ramdisk NVWC for each admission policy. ALL and SYNC gradually improves the performance as the NVWC capacity increases whereas the other admission policies show nearly constant performance in all cases. This is because ALL and SYNC generates lots of stalled writes in the case of small NVWC capacities; up to 58\% and 45\% for ALL and SYNC each. Consequently, CP+PCI+IOCI could gain up to 80\% and 61\% performance improvement over ALL and SYNC, respectively. Though WAL, CP, and CP+PCI did not suffer from write stalls at all in all the cases, they achieved still lower performance than CP+PCI+IOCI due to complex dependencies as described in Section~\ref{sec:DesignIssue}. 

\begin{figure}
\centering
\epsfig{file=figures/eval-analysis-postgres-ramdisk-iops.eps, width=3in}
\caption{\textbf{System performance and caching efficiency.} \textit{We measured the number of 4KB I/Os for both ramdisk and two HDDs in the case of 512MB NVWC. Performance per cached block is calculated by dividing the measured tpmC by the number of 4KB cached blocks.}}
\label{fig:eval-analysis-postgres-ramdisk-iops}
\end{figure}

Interestingly, we found the inconsistency between storage system performance (i.e., IOPS) and application performance (i.e., tpmC). For instance, in the 512MB NVWC case, ALL better utilized NVWC by 40\% than CP+PCI+IOCI leading to achieve 10\% higher system performance as shown in Figure~\ref{fig:eval-analysis-postgres-ramdisk-iops}. However, CP+PCI+IOCI accomplished 57\% higher application performance than that of ALL because CP+PCI+IOCI could utilize NVWC more efficiently in terms of application performance. This result validates our argument on necessity of distinguishing criticality of each write for application performance.

\begin{figure}
\centering
\epsfig{file=figures/eval-analysis-postgres-wait-time.eps, width=3in}
\caption{\textbf{PostgreSQL dependency analysis.} \textit{We measured latencies of PostgreSQL backends using LatencyTOP in the case of 4GB ramdisk as an NVWC device. We omit network-related latencies for brevity.}}
\label{fig:eval-analysis-postgres-ramdisk-dependency}
\end{figure}

To further analyze the reason behind the performance differences, we measured the system latencies of backend processes for each admission policy in the 4GB NVWC case. As shown in Figure~\ref{fig:eval-analysis-postgres-ramdisk-dependency}, ALL could eliminate most of the latencies because the capacity of NVWC was enough not to stall any writes and the bandwidth of ramdisk was sufficient to absorb whole write traffic. On the other hand, SYNC causes excessive latencies because asynchronous writes requested by kernel thread for cleaning OS buffer cache makes dependencies to a critical process. Similarly, WAL and CP make critical processes suffer from excessive latencies caused by the write traffic that does not related to log files. CP+PCI could further reduce latencies by resolving process dependencies. CP+PCI+IOCI could eliminate most of the latencies since it additionally handles I/O dependencies. As a consequence, CP+PCI+IOCI could achieve the highest level of application performance in all the capacities.

\begin{table}
\begin{center}
\begin{footnotesize}
	\centering
	\begin{tabular}{|c|c|r|r|r|r|}
		\cline{3-6}
		\multicolumn{2}{r|}{} & \multicolumn{4}{c|}{\textbf{Applied Technique}} \\
		\cline{2-6}
		\multicolumn{1}{r|}{} & Ratio (\%) & CP & PCI & IOCI & SUM \\
		\hline
		\multirow{5}{*}{\textbf{Data}} & Data (DB) & 0 & 0.5209 & 0.0515 & 0.5723 \\ \cline{2-6}
					& Data (LOG) & 98.7409 & 0.5707 & 0.0007 & 99.3123 \\ \cline{2-6}
					& Metadata & 0 & 0 & 0.0014 & 0.0014 \\ \cline{2-6}
					& Journal & 0 & 0.0782 & 0.0357 & 0.1140 \\ \cline{2-6}
					& SUM & 98.7409 & 1.1698 & 0.0893 & 100 \\
		\hline\hline
		\multirow{3}{*}{\textbf{IO}} & Sync. & 98.7409 & 1.0312 & 0.0357 & 99.8078 \\ \cline{2-6}
					& Async. & 0 & 0.1387 & 0.0535 & 0.1922 \\ \cline{2-6}
					& SUM & 98.7409 & 1.1698 & 0.0893 & 100 \\
		\hline
	\end{tabular}
\end{footnotesize}
\end{center}
\caption{\textbf{Breakdown of critical writes.} \textit{We collected traces at block-level during the execution of the TPC-C benchmark with CP+PCI+IOCI and 4GB ramdisk NVWC.}}
\label{tab:tpcc-cached-data-breakdown}
\end{table}

To help understand which types of data and I/O constitute critical writes, Table~\ref{tab:tpcc-cached-data-breakdown} shows the breakdown of critical writes in terms of data and I/O types. As we expected, the dominant type of data comprising critical writes is transaction logs that are requested synchronously by backends during transaction commits. However, the rest of the critical writes is still crucial since it contributed to additional 52\% performance improvement compared to CP alone. On the other side, the dominant type of I/O comprising critical writes is synchronous writes. Though the portion of asynchronous writes is insignificant, it contributed to additional 38\% performance improvement compared to SYNC. Overall, dependency-induced critical writes have significant impact on application performance even though they are insignificant amount in total.

\begin{figure}
\centering
\epsfig{file=figures/eval-performance-postgres-ssd-capacity.eps, width=3in}
\caption{\textbf{PostgreSQL performance with SSD.} \textit{}}
\label{fig:eval-performance-postgres-ssd}
\end{figure}

\begin{figure}
\centering
\epsfig{file=figures/eval-analysis-postgres-ssd-iops.eps, width=3in}
\caption{\textbf{System performance and NVWC write latency.} \textit{We measured the number of 4KB I/Os for both SSD and two HDDs in the case of 16GB NVWC to calculate IOPS. We report average 4KB synchronous write latency of critical processes as NVWC write latency.}}
\label{fig:eval-analysis-postgres-ssd-iops}
\end{figure}

\begin{figure*}
\centering
\subfigure[Redis performance\label{fig:eval-performance-redis-ssd}]{\epsfig{file=figures/eval-performance-redis-ssd.eps, width=3in}}
\subfigure[Caching efficiency\label{fig:eval-analysis-redis-ssd-efficiency}]{\epsfig{file=figures/eval-analysis-redis-ssd-efficiency.eps, width=3in}}
\caption{\textbf{Redis performance analysis.} \textit{We used 16GB SSD as an NVWC device. We report YCSB throughput normalized to ALL. Performance per cached block is calculated by dividing the measured ops/s by the number of 4KB cached blocks.}}
\label{fig:eval-analysis-redis-ssd}
\end{figure*}

\begin{figure*}
\centering
\subfigure[Workload A\label{fig:eval-analysis-redis-ssd-cdf-a}]{\epsfig{file=figures/eval-analysis-redis-ssd-cdf-a.eps, width=3in}}
\subfigure[Workload B\label{fig:eval-analysis-redis-ssd-cdf-b}]{\epsfig{file=figures/eval-analysis-redis-ssd-cdf-b.eps, width=3in}}
\caption{\textbf{Redis tail latency.} \textit{We omit the results of Workload D and F due to space constraint.}}
\label{fig:eval-analysis-redis-ssd-cdf}
\end{figure*}

Next, Figure~\ref{fig:eval-performance-postgres-ssd} shows the TPC-C throughput averaged over three runs with varying capacity of SSD NVWC for each admission policy. Contradict to the case of ramdisk NVWC, ALL and SYNC achieved lower performance than the criticality-aware policies in all the capacities. This is because they directed most of the write traffic to SSD making the critical writes contending with the non-critical writes for the limited bandwidth of SSD (Figure~\ref{fig:eval-analysis-postgres-ssd-iops}). Though SYNC could ease contention in the SSD more than ALL, it still had dependency problems caused by asynchronous writes. On the other hand, WAL and CP utilized both the SSD and the HDDs more efficiently, thereby improving application performance compared to ALL and SYNC. CP+PCI+IOCI shows the highest level of performance in all the capacities because it effectively controlled congestion of the SSD by solely utilizing SSD NVWC for a set of critical writes.

% By adjusting checkpoint frequency, we can ease background write traffic at the expense of increased recovery time. Experiments with 512MB ramdisk.

\subsection{Redis with YCSB}

For Redis, we enabled both snapshotting (i.e., RDB) and command logging (i.e., AOF) according to the suggestion in the official document~\cite{RedisPersistence}. To utilize our multi-core testbed, we concurrently ran four YCSB benchmarks against four Redis instances since Redis exploits single threaded design that serves all the requests sequentially by a single server thread in order to keep the code complexity as low as possible~\cite{RedisLatency}. Due to the space constraint, we present only the case of SSD as an NVWC device.

Figure~\ref{fig:eval-performance-redis-ssd} demonstrates average YCSB throughput for each admission policy with 16GB SSD NVWC normalized to ALL. ALL achieves the lowest level of performance since it did not handle congestion in the request queues of the SSD caused by background snapshotting and log rewriting, thereby delaying the processing of critical writes. SYNC further improves the performance by filtering out asynchronous writes issued by kernel thread to clean OS buffer cache. However, the improvement gained by SYNC cannot be guaranteed because the total amount of asynchronous writes depends on the writeback policy of OS buffer cache. By providing the first class support for critical writes, CP+PCI+IOCI outperforms by 47-90\% compared to ALL and 29-40\% compared to SYNC. Moreover, CP+PCI+IOCI dramatically improves NVWC caching efficiency by up to 20x and 8x than ALL and SYNC each as shown in Figure~\ref{fig:eval-analysis-redis-ssd-efficiency}.

To show the impact of different admission policies on tail latency, we also measured the latency distribution of YCSB requests. As plotted in Figure~\ref{fig:eval-analysis-redis-ssd-cdf}, only CP+PCI+IOCI could keep $99.9^{th}$ and $99.99^{th}$-percentile latencies below 100ms, which makes users feel more consistently responsive than higher latencies~\cite{Card1991}, for both workloads. ALL and SYNC, on the other hand, increased the $99.9^{th}$-percentile latency by an order of magnitude compared to that of CP+PCI+IOCI for both workloads. Moreover, the $99.99^{th}$-percentile latencies of ALL and SYNC exceeded one second, which is the maximum latency measured by YCSB, for both workloads. Though Workload B is consisted mostly of read request, the tail latencies largely affected by the admission policy used because the single threaded design of Redis delayed the processing of read requests that are queued behind update requests. Note that Workload D and F showed the similar trend of tail latency to that of Workload A. Considering the significance of consistent responses to users~\cite{Brutlag2009,Schurman2009} especially for large-scale Web services~\cite{Dean2013,Timothy2014}, this result indicates that the criticality-aware write classification is vital for providing high quality services to users.

% workload A: ALL,SYNC, and CP+PCI+IOCI 99th 80ms, 72ms, and 32ms, 99.9th 649ms, 678ms, and 50ms, 99.99th exceeds one second, 79ms.
% workload B: ALL,SYNC, and CP+PCI+IOCI 99th 70ms, 59ms, and 32ms, 99.9th 572ms, 438ms,and 62ms, 99.99th exceeds one second, 83ms.

%\section{Discussion}

%\subsection{Impact on write-through cache}

%\subsection{Application}

%Real time systems

%Interactive systems (desktop and mobile)

%Client-side flash cache - Which data need or need not to be admitted to client-side flash cache?

%\subsection{OS-level Identification of Critical Process}

%Typical server applications spawn a server process in respond to a user request via network.

%About read inclusive caching incurs unnecessary write traffic to non-volatile cache because storage read always accompanies cache write even though that block also will be placed OS cache. Contention, queue congestion increase in a cache device.

%Recovery time penalty vs. conventional physiological logging: While command logging has a longer recovery time, it's impact on availability is minimal because all modern production OLTP systems are engineered to employ replication for high availability, so that the higher throughput of command logging at run-time is a good tradeoff for it's slower background recovery while the other replica nodes continue to serve live traffic.

%\begin{table*}[!t]
%\centering
%\begin{footnotesize}
%    \begin{tabular}{|c|c|r|r|r|r|r|r|}
%            \hline
%            \multicolumn{2}{|c|}{} &    \multicolumn{3}{c|}{Update} & \multicolumn{3}{c|}{Read} \\
%            \cline{3-8}
%            \multicolumn{2}{|c|}{} & ALL    & SYNC  & CP+PCI+IOCI   & ALL   & SYNC & CP+PCI+IOCI \\
%            \hline
%            \hline
%            \multirow{5}{*}{RAMDISK}
%                & AVG & 1.20 & 1.21 & 1.13 & 1.21 & 1.23 & 1.15 \\
%                & MIN & 0.22 & 0.23 & 0.23 & 0.13 & 0.14 & 0.13 \\
%                & 95$^{th}$ percentile & 1 & 1 & 1 & 1 & 1 & 1 \\
%                & 99$^{th}$ percentile & 3 & 3 & 3 & 3 & 3 & 3 \\
%                & MAX & 2297.29 & 2523.67 & 135.01 & 2297.28 & 2523.71 & 135.19 \\
%            \hline
%            \multirow{5}{*}{SSD}
%                & AVG & 15.69 & 14.64 & 13.52 & 15.58 & 14.58 & 13.45 \\
%                & MIN & 4.66 &  4.65 &  4.62 &  0.16 &  0.16 &  0.14 \\
%                & 95$^{th}$ pecentile & 23 & 21 &   20 &    23 &    21 &    20 \\
%                & 99$^{th}$ pecentile & 39 & 33 &   26 &    39 &    33 &    26 \\
%                & MAX & 5003.15 &   4767.15 &   361.3 & 5003.32 &   4767.17 &
%                361.29 \\
%        \hline
%    \end{tabular}
%\end{footnotesize}
%\caption{\textbf{YCSB operation latency.} \textit{Average, min, max. 512MB ramdisk and 16GB SSD, workload A, 40 clients, 1 GB dataset, 10 m operations}}
%\label{tab:ycsb-latency}
%\end{table*}

%\begin{figure*}
%\centering
%\subfigure[Performance scalability\label{fig:eval-performance-postgres-ssd-scalaiblity}]{\epsfig{file=figures/eval-performance-postgres-ssd-scalability.eps, width=3in}}
%\subfigure[Performance per cached blocks\label{fig:eval-analysis-postgres-ssd-efficiency}]{\epsfig{file=figures/eval-analysis-postgres-ssd-efficiency.eps, width=3in}}
%\caption{\textbf{PostgreSQL scalability.} \textit{16GB SSD NVWC. Caching efficiency measured via TPC-C throughput per cached blocks.}}
%\label{fig:eval-analysis-postgres-ssd-scalability}
%\end{figure*}

%Figure~\ref{fig:eval-analysis-postgres-ssd-scalability}. ramdisk case shows the similar trend.

