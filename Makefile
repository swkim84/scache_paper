NAME	= scache-critical
TEXS	:= scache-critical.tex introduction.tex background_motivation.tex design.tex implementation.tex evaluation.tex relatedwork.tex conclusion.tex

$(NAME).pdf: $(NAME).ps
	ps2pdf14 -dPDFSETTINGS=/prepress -dEmbedAllFonts=true $<

$(NAME).ps: $(NAME).dvi
	dvips -P cmz -t letter -o $(NAME).ps $(NAME).dvi

$(NAME).dvi: $(TEXS) $(NAME).bib
	latex $(NAME).tex
	bibtex $(NAME)
	latex $(NAME).tex
	latex $(NAME).tex

$(NAME).rtf: $(TEXS) $(NAME).bbl $(NAME).aux
	latex2rtf -a $(NAME).aux -b $(NAME).bbl -F $(NAME).tex

clean:
	rm -f $(NAME).aux $(NAME).bbl $(NAME).blg $(NAME).dvi $(NAME).lof $(NAME).log $(NAME).lot $(NAME).pdf $(NAME).ps $(NAME).toc
