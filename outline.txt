1. Introduction

2. Background and Motivation

 2.1 Non-volatile Write Caches

  - NV-DRAM chracteristics

  - Flash characteristics

  - SCM characteristics

 2.2 Why Blindly Caching All the Writes is a Bad Idea?

  - Frequent stalled writes in a performance-critical path

  - Excessive queueing delay for performance-critical writes

  - Reduced lifetime and performance depending on the characteristic of NVWC 

  - Not all writes is performance-critical

3. Which Type of Write is Performance-Critical?

 - Our target enviroment? client application - server application

 3.1 I/O Clsssification in the Client's Viewpoint

  - Satisfying clients is the main concern in typical server applications

  - A simple approach: synchronous writes is performance-critical

   * Definition of synchronous writes

   * Limitation of this approach

  - Writes awaited by client to be completed are performance-critical

   * We refer to this type of write as critical write for simplicity

  - The main benefits of the proposed classification

 3.2 Dynamic Dependency

  - Dependency to subject (process, thread)

  - Dependency to object (page, buffer, I/O)

  - User- and kernel-level dependency

  - Experimental study for revealing the severity of dynamically-generated dependencies

4. Detection of Performance-Critical Write

 - Criticality inheritance for handling the dependency to subject

 - Dynamic rerouting for handling the dependency to object

 4.1 Criticality Inheritance 

  - Typical architecture of server applications

   * Foreground processes (UI thread in interactive system) and background processes

   * Why this architecture is common?

  - Foreground process as critical process (the entity requires first class support)

   * Disclosing hint: pros - portable, easy to use, cons - hint abuse

   * Application modification vs OS only approach

  - Writes awaited by a critical process is performance-critical

 4.2 Dynamic Rerouting of Non-critical Writes

 4.3 Wait Status Tracking

5. Implementation

 5.1 Kernel Modifications

  - Interfaces for applications

  - Block-level caching layer

 5.2 Application Modifications

  - PostgreSQL

  - Redis

6. Evaluation

 5.1 Methodology

  - Workloads
   
   * TPC-C for PostgreSQL, YCSB for Redis

  - Policies

   * ALL, SYNC, WAL, CRITICAL

  - Metric

   * Caching efficiency (client-perceived performance / cached blocks)

  - NVWC device

   * RAMDISK: clflush_cache_range()

   * Flash SSD: discarding (TRIMing) all blocks (and sleeps 3 min) before starting workloads

 5.2 Results

  - TPC-C (PostgreSQL)

   * NVWC capacity

   * # of concurrent clients

   * Checkpoint frequency (background write traffic)

  - YCSB (Redis)

   * NVWC capacity

   * # of concurrent clients

   * Latency distribution

6. Related Work

 6.1

 6.2

7. Conclusion and Future Direction

 - Findings and impacts

 - Future work

