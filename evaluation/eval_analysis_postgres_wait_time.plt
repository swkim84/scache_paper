#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 26
#set size 1,1
#set key invert reverse left Left width -1
set key invert top vertical width -1 height 0 samplen 2 font "Times-Roman, 22"
set ylabel 'Total wait time (min)' offset 2,0 
#set xrange [-0.3:2.3]
set yrange [0:200]
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -30
set xtics ("ALL" 0, "SYNC" 1, "WAL" 2, "CP" 3, "CP+PI" 4, "CP+PI+IOI" 5) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
set ytics 0,50
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.7

set size 1,0.8
set output '../figures/eval-analysis-postgres-wait-time.eps'
plot 'eval_analysis_postgres_wait_time.dat' using ($2/1000000/60) t 'mutex\_lock' lt 1 fs solid 0.10, '' using ($3/1000000/60) t 'sleep\_on\_shadow\_bh'  lt 1 fs pattern 1, '' using ($4/1000000/60) t 'wait\_on\_page\_writeback'  lt 1 fs solid 0.40, '' using ($5/1000000/60) t 'wait\_transaction\_locked'  lt 1 fs pattern 4, '' using ($6/1000000/60) t 'etc' lt 1 fs solid 0.80

set yrange [0:250]
set output '../figures/eval-analysis-postgres-wait-time-512MB.eps'
plot 'eval_analysis_postgres_wait_time_512MB.dat' using ($2/1000000/60) t 'mutex\_lock' lt 1 fs solid 0.10, '' using ($3/1000000/60) t 'sleep\_on\_shadow\_bh'  lt 1 fs pattern 1, '' using ($4/1000000/60) t 'wait\_on\_page\_writeback'  lt 1 fs solid 0.40, '' using ($5/1000000/60) t 'wait\_transaction\_locked'  lt 1 fs pattern 4, '' using ($6/1000000/60) t 'etc' lt 1 fs solid 0.80
