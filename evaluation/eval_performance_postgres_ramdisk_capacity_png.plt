#!/opt/local/bin/gnuplot

set terminal png enhanced font 'Times-Roman' 22
set ylabel "Avg TPC-C thput (KtpmC)" offset 1.5,0
set xlabel "NVWC capacity (MB)" #offset 1.5,0

set style data histograms
set style histogram
set style histogram errorbars lw 2
set style fill solid border -1
set boxwidth 1

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -2 height 1 samplen 2 font "Times-Roman, 18"

set yrange [0:45]
set xrange [-0.6:5.7]
set xtics nomirror
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.8
set output "../figures/eval-performance-postgres-ramdisk-capacity.png"
plot "eval_performance_postgres_ramdisk_capacity.dat" using ($2/1000):($8/1000):xtic(1) lc 0 fs solid 0.70 title columnheader(2), \
	"" using ($3/1000):($9/1000):xtic(1) lc 1 fs solid 0.7 title columnheader(3),\
	"" using ($4/1000):($10/1000):xtic(1) lc 2 fs solid 0.7 title columnheader(4),\
	"" using ($5/1000):($11/1000):xtic(1) lc 3 fs solid 0.7 title columnheader(5),\
	"" using ($6/1000):($12/1000):xtic(1) lc 4 fs solid 0.7 title columnheader(6), \
	"" using ($7/1000):($13/1000):xtic(1) lc 5 fs solid 0.7 title columnheader(7)
