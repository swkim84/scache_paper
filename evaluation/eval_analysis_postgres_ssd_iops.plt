#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome font 'Times-Roman' 24
#set size 0.6,0.6
#set key invert reverse left Left width -1
set key invert top horizontal outside width -2 height 0.7 samplen 2 font "Times-Roman, 24"
set ylabel '4KB IOPS (x1000)' offset 1,0 
#set xrange [-0.3:2.3]
set yrange [0:25]
set y2range [0:10]
set y2label 'Avg NVWC write latency (ms)' offset -1,0
set y2tics nomirror 0,2
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -30
set xtics ("ALL" 0, "SYNC" 1, "WAL" 2, "CP" 3, "CP+PI" 4, "CP+PI+IOI" 5) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,2000
set ytics nomirror 0,5
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5

set size 1,0.8
set output '../figures/eval-analysis-postgres-ssd-iops.eps'
plot 'eval_analysis_postgres_ssd_iops.dat' using ($2/(30*60)/1000) t 'SSD IOPS' lt 1 fs solid 0.20, '' using ($3/(30*60)/1000) t 'HDDs IOPS'  lt 1 fs solid 0.6, '' using 4 title 'Avg write latency'  with linespoints lt 1 pt 5 ps 1.5 lw 3 axis x1y2
