#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 22
set ylabel "New order transactions / minute (tpmC)" #offset 1.5,0
set xlabel "Log size limit (MiB)" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -2 height 1 samplen 2 font "Times-Roman, 20"

set yrange [0:]
set xrange [-0.5:4.5]
set xtics nomirror
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-performance-postgres-ssd-128GB-checkpoint-frequency.eps"
plot for [i=2:6] "eval_performance_postgres_ssd_128GB_checkpoint_frequency.dat" using i:xtic(1) with linespoints ls 1 pt (i-1) title columnheader(i)

#set output "../figures/eval-performance-postgres-ramdisk-4GB-scalability.eps"
#plot for [i=2:6] "eval_performance_postgres_ramdisk_4GB_scalability.dat" using i:xtic(1) with linespoints ls 1 pt (i-1) title columnheader(i)

