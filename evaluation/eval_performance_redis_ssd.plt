#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "Normalized YCSB throughput" offset 1,0
set xlabel "Type of workload" #offset 1.5,0

set style data histograms
set style histogram
set style histogram errorbars lw 2
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal width -1 height 1 samplen 2 font "Times-Roman, 22"

set yrange [0:2.5]
set xrange [-0.5:3.7]
set xtics nomirror
set ytics nomirror

set grid y
set format y '%.1f'
#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-performance-redis-ssd.eps"
plot "eval_performance_redis_ssd.dat" using 2:7:xtic(1) fs solid 0.1 title columnheader(2) ls 1,\
	"" using 3:8:xtic(1) fs pattern 2 title columnheader(3) ls 1, \
	"" using 4:9:xtic(1) fs solid 0.4 title columnheader(4) ls 1,\
	"" using 5:10:xtic(1) fs pattern 4 title columnheader(5) ls 1, \
	"" using 6:11:xtic(1) fs solid 0.8 title columnheader(6) ls 1
