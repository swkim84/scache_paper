#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "Perf. per cached block (x1000)" offset 1,0
set xlabel "NVWC capacity (MB)" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width 0 height 1 samplen 2 font "Times-Roman, 22"

set yrange [0:]
set xrange [-0.5:5.5]
set xtics nomirror
set ytics nomirror
set xtics ('32' 0, '128' 1, '512' 2, '1024' 3, '2048' 4, '4096' 5)

set grid y

set format y "%.1f"
#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1,0.8
set output "../figures/eval-analysis-postgres-ramdisk-efficiency.eps"
plot "eval_analysis_postgres_ramdisk_efficiency.dat" using 1:2:8 with yerrorlines lw 2 lt 1 pt 1 ps 2 title 'ALL', '' using 1:3:9 with yerrorlines lw 2 lt 2 pt 2 ps 2 title 'SYNC', '' using 1:4:10 with yerrorlines lw 2 lt 3 pt 3 ps 2 title 'WAL', '' using 1:5:11 with yerrorlines lw 2 lt 4 pt 4 ps 2 title 'CP', '' using 1:6:12 with yerrorlines lw 2 lt 5 pt 5 ps 2 title 'CP+PI', '' using 1:7:13 with yerrorlines lw 2 lt 6 pt 6 ps 2 title 'CP+PI+IOI'
