#!/opt/local/bin/gnuplot

set ylabel "Normalized YCSB throughput" offset 0,0
set xlabel "Type of workload" offset 0,-0.5

set style data histograms
set style histogram
set style histogram errorbars lw 2
set style fill solid border -1
set boxwidth 1

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top outside horizontal width -2 height 1 samplen 2 font "Times-Roman, 18"

set yrange [0:1.6]
set xrange [-0.5:3.7]
set xtics nomirror scale 0
set ytics nomirror 0,0.2

set label "Ramdisk" at graph 0.25,first -0.34 center
set label "SSD" at graph 0.73,first -0.34 center
set arrow 1 from graph 0,-0.13 to graph 0,0 nohead
set arrow 2 from graph 0.5,-0.1 to graph 0.5,0 nohead
set arrow 3 from graph 1,-0.13 to graph 1,0 nohead

set grid y
set format y '%.1f'
#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.8
set terminal png enhanced font 'Times-Roman' 22
set output "../figures/eval-performance-redis.png"
plot "eval_performance_redis.dat" \
       using 2:7:xtic(1) lc 0 fs solid 0.7 title columnheader(2),\
	"" using 3:8:xtic(1) lc 1 fs solid 0.7 title columnheader(3), \
	"" using 4:9:xtic(1) lc 2 fs solid 0.7 title columnheader(4),\
	"" using 5:10:xtic(1) lc 3 fs solid 0.7 title columnheader(5), \
	"" using 6:11:xtic(1) lc 4 fs solid 0.7 title columnheader(6)
