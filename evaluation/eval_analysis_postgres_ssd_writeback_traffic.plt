#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 22
set ylabel "# of writeback (in millions)" offset 1.5,0
set xlabel "NVWC capacity (GB)" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width 0 height 1 samplen 2 font "Times-Roman, 20"

set yrange [0:]
#set xrange [-0.5:7]
set xtics nomirror
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-analysis-postgres-ssd-writeback-traffic.eps"
plot "eval_analysis_postgres_ssd_writeback_traffic.dat" using 2:xtic(1) fs pattern 1 title columnheader(2) ls 1, \
	"" using 3:xtic(1) fs solid 0.1 title columnheader(3) ls 1,\
	"" using 4:xtic(1) fs pattern 2 title columnheader(4) ls 1, \
	"" using 5:xtic(1) fs solid 0.4 title columnheader(5) ls 1,\
	"" using 6:xtic(1) fs pattern 4 title columnheader(6) ls 1, \
	"" using 7:xtic(1) fs solid 0.8 title columnheader(7) ls 1

