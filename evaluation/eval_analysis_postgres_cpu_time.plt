#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 25
#set size 1,1
#set key invert reverse left Left width -1
set key top horizontal outside width -2 height 1 samplen 2 font "Times-Roman, 20"
set ylabel 'CPU utilization (%)' offset 2,0 
#set xrange [-0.3:2.3]
set yrange [0:100]
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -45
set xtics ("ALL" 0, "SYNC" 1, "WAL" 2, "CP" 3, "CP+PCI" 4, "CP+PCI+IOCI" 5) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,2000
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5

set output '../figures/eval-analysis-postgres-cpu-time.eps'
plot 'eval_analysis_postgres_cpu_time.dat' using 2 t 'user' lt 1 fs solid 0.10, '' using 3 t 'system'  lt 1 fs pattern 1, '' using 4 t 'idle'  lt 1 fs solid 0.40, '' using 5 t 'iowait'  lt 1 fs pattern 4
