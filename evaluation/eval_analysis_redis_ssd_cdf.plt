#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "CDF (%ile)" offset 1,0
set xlabel "Request latency (ms)" #offset 0,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width -1.5 height 1 samplen 2 font "Times-Roman, 22"

set yrange [99:100]
set xrange [0:1000]
set xtics nomirror 0,100
set ytics nomirror 0,0.1

set format y '%.1f'
set grid y
set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1,0.8
set output "../figures/eval-analysis-redis-ssd-cdf-a.eps"
plot "eval_analysis_redis_ssd_cdf_a.dat" using ($2*100) with lines ls 1 lw 4 title columnheader(2), "" using ($3*100) with lines ls 2 lw 4 title columnheader(3), "" using ($4*100) with lines ls 5 lw 4 title columnheader(4)

set output "../figures/eval-analysis-redis-ssd-cdf-b.eps"
plot "eval_analysis_redis_ssd_cdf_b.dat" using ($2*100) with lines ls 1 lw 4 title columnheader(2), "" using ($3*100) with lines ls 2 lw 4 title columnheader(3), "" using ($4*100) with lines ls 5 lw 4 title columnheader(4)

set output "../figures/eval-analysis-redis-ssd-cdf-d.eps"
plot "eval_analysis_redis_ssd_cdf_d.dat" using ($2*100) with lines ls 1 lw 4 title columnheader(2), "" using ($3*100) with lines ls 2 lw 4 title columnheader(3), "" using ($4*100) with lines ls 5 lw 4 title columnheader(4)

set output "../figures/eval-analysis-redis-ssd-cdf-f.eps"
plot "eval_analysis_redis_ssd_cdf_f.dat" using ($2*100) with lines ls 1 lw 4 title columnheader(2), "" using ($3*100) with lines ls 2 lw 4 title columnheader(3), "" using ($4*100) with lines ls 5 lw 4 title columnheader(4)
