#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 25
set output '../figures/motiv-thput-gap.eps'
#set size 0.95,1
#set bmargin 3.5
#set auto x
#set key invert reverse left Left width -1
set ylabel '4KB IOPS' 
set xlabel '# of clients' 
set y2label 'Transactions / 4KB cached blocks (10^3)' 
#set xrange [-0.5:6]
set yrange [0:15000]
set y2range [0:35]
#set xtics 0,10
set xtics nomirror
#set xtic rotate by -45
#set xtics ("32" 0, "128" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
#set ytics 0,20
set ytics 0,3000
set ytics nomirror
set y2tics 0,7
#set style data histograms
#set style histogram rowstacked
set grid y
#set boxwidth 0.35
plot 'motiv_thput_gap.dat' using 2:xtic(1) title columnhead with linespoints lt 2 pt 2 lw 3, '' using 3:xtic(1) title columnhead with linespoints lt 1 pt 5 lw 3 axis x1y2
