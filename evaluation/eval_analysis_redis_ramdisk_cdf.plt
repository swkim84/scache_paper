#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 22
set ylabel "CDF" #offset 1.5,0
set xlabel "Latency (ms)" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -1 height 1 samplen 2 font "Times-Roman, 20"

set yrange [0.90:1.02]
set xrange [0:40]
set xtics nomirror
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-analysis-redis-ramdisk-cdf.eps"
plot for [i=2:4] "eval_analysis_redis_ramdisk_cdf.dat" using i:xtic(1) with lines ls 1+i title columnheader(i)

