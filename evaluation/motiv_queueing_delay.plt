#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome 'Times-Roman' 30
set output '../figures/motiv-queueing-delay.eps'
#set size 0.95,1
#set bmargin 3.5
#set auto x
#set key invert reverse left Left width -1
set key invert font 'Times-Roman, 26' samplen 2
set ylabel 'Normalized TPC-C thput' offset 1,0
set xlabel '# of clients' offset 0,0.3 
set y2label 'Avg length of OS request Q' offset -1.5,0
#set xrange [-0.5:6]
set yrange [0.00:1.4]
set y2range [0.00:3500.00]
#set xtics 0,10
set xtics nomirror font 'Times-Roman, 26'
#set xtic rotate by -45
#set xtics ("32" 0, "128" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
#set ytics 0,20
set ytics 0,0.2
set ytics nomirror
set y2tics 0,500
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.4
set format y '%.1f'
set size 1,0.8
plot 'motiv_queueing_delay.dat' using 2:xtic(1) title columnhead fs solid 0.3 lt 1 axis x1y2, '' using 3:xtic(1) title columnhead with linespoints lt 1 pt 3 lw 2 ps 2
