#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 22
set ylabel "Performance per cached blocks (x10^3)" offset 1,0
set xlabel "Type of workload" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal width -1 height 0 samplen 2 font "Times-Roman, 20"

set yrange [0:]
set xrange [-0.5:3.7]
set xtics nomirror
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-analysis-redis-ramdisk-efficiency.eps"
plot "eval_analysis_redis_ramdisk_efficiency.dat" using 2:xtic(1) fs solid 0.1 title columnheader(2) ls 1,\
	"" using 3:xtic(1) fs solid 0.4 title columnheader(3) ls 1, \
	"" using 4:xtic(1) fs solid 0.8 title columnheader(4) ls 1

