#!/opt/local/bin/gnuplot

set terminal png enhanced font 'Times-Roman' 22
set ylabel "YCSB throughput (Kops/sec)" offset 1,0
set xlabel "Elapsed time (min)" offset 0,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width 0 height 1 samplen 2 font "Times-Roman, 18"

set yrange [0:12]
#set xrange [0:120]
set xtics nomirror 0,5
set ytics nomirror

#set format y '%.1f'
set grid y
#set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.8
set output "../figures/eval-performance-redis-ssd-timeline.png"
plot "eval_performance_redis_ssd_timeline.dat" using ($1/60):($2/1000) with lines lc 1 lw 2 title columnheader(2), "" using ($1/60):($3/1000) with lines lc 3 lw 2 title columnheader(3)
