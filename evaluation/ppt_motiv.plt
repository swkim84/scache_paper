#!/usr/bin/gnuplot

set terminal png enhanced font 'Times-Roman' 24
#set size 0.6,0.6
#set key invert reverse left Left width -1
set key invert top horizontal outside width 0 height 0.7 samplen 2 font "Times-Roman, 20"
set ylabel '4KB IOPS (x1000)' offset 1,0 
#set xrange [-0.3:2.3]
set yrange [0:36]
set y2range [0:60]
set y2label 'Transactions/min (x1000)' offset 0,0
set y2tics nomirror 0,10
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -30
#set xtics ("ALL" 0, "SYNC" 1, "WAL" 2, "CP" 3, "CP+PI" 4, "CP+PI+IOI" 5) font "Times-Roman, 22"
set xtics ("No NVWC" 0, "NV-DRAM" 1, "Flash SSD" 2) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,2000
set ytics nomirror 0,6
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5

#set size 1,0.8
set output '../figures/ppt-motiv.png'
plot 'ppt_motiv.dat' using ($2/1000) t 'IOPS' lc 0 fs solid 0.7, '' using ($3/1000) title 'Trx/min' with linespoints lc -1 pt 5 ps 2.5 lw 5 axis x1y2
#plot 'ppt_motiv.dat' using ($2/1000) t 'IOPS' lc 0 fs solid 0.7
