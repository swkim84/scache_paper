#!/usr/bin/gnuplot

set terminal png enhanced font 'Times-Roman' 26 
#set size 1,1
#set key invert reverse left Left width -1
set key outside invert width -7 height 0 samplen 2 font "Times-Roman, 22"
set ylabel 'Total wait time (min)' offset 2,0 
set y2label 'TPC-C throughput (KtpmC)' offset 0,0 
set xrange [-0.7:2.7]
set yrange [0:150]
set y2range [0:50]
#set xtics 0,200000
set xtics nomirror
#set xtic rotate by -45
set xtics ("No NVWC" 0, "CP" 1, "Optimal" 2) font "Times-Roman, 24"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
set ytics 0,30
set y2tics 0,10
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.6
#set size 1.1,0.6
set output '../figures/design-analysis-dependency.png'
#plot 'design_analysis_dependency.dat' using ($2/60) t 'inode mutex' lc 1 fs solid 0.70, '' using ($5/60) t 'fs transaction lock'  lc 4 fs solid 0.70, '' using ($4/60) t 'sync write'  lc 3 fs solid 0.70, '' using ($3/60) t 'fs journal write'  lc 2 fs solid 0.7, '' using ($6/60) t 'etc' lc 5 fs solid 0.70
plot 'design_analysis_dependency.dat' using ($2/60) t 'inode mutex' lc 1 fs solid 0.70, '' using ($5/60) t 'fs transaction lock'  lc 4 fs solid 0.70, '' using ($4/60) t 'sync write'  lc 3 fs solid 0.70, '' using ($3/60) t 'fs journal write'  lc 2 fs solid 0.7, '' using ($6/60) t 'etc' lc 5 fs solid 0.70, '' using ($7/1000) t 'TPC-C thput' with linespoints lc -1 pt 3 lw 2 ps 2 axis x1y2 
#plot 'design_analysis_dependency.dat' using ($2/60) notitle lc 1 fs solid 0.70, '' using ($5/60) notitle  lc 4 fs solid 0.70, '' using ($4/60) notitle  lc 3 fs solid 0.70, '' using ($3/60) notitle  lc 2 fs solid 0.7, '' using ($6/60) notitle lc 5 fs solid 0.70 
#plot 'design_analysis_dependency.dat' using ($2/60) notitle lc 1 fs solid 0.70, '' using ($5/60) notitle  lc 4 fs solid 0.70, '' using ($4/60) notitle  lc 3 fs solid 0.70, '' using ($3/60) notitle  lc 2 fs solid 0.7, '' using ($6/60) notitle lc 5 fs solid 0.70, '' using ($7/1000) notitle with linespoints lc -1 pt 3 lw 2 ps 2 axis x1y2 

