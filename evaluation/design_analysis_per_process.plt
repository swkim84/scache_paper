#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 25
#set size 1,1
#set key invert reverse left Left width -1
set key invert top vertical samplen 2 font "Times-Roman, 22"
set ylabel '# of 4KB writes (in millions)' offset 2,0 
#set xrange [-0.3:2.3]
#set yrange [0:12000]
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -20
set xtics ("Backends (user)" 0, "Checkpointer (user)" 1, "Kworker (kernel)" 2, "Etc (user+kernel)" 3) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,2000
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5

set output '../figures/design-analysis-per-process.eps'
plot 'design_analysis_per_process.dat' using 2 t 'Async.' lt 1 fs solid 0.10, '' using 3 t 'Sync.' lt 1 fs solid 0.60
