#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 22
set ylabel "Log size (GB)" #offset 1.5,0
set xlabel "Elapsed time (s)" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -3 height 1 samplen 2 font "Times-Roman, 18"

set yrange [0:]
#set xrange [0:40]
#set xtics nomirror
unset xtics
set ytics nomirror

set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set format y "%.1f"
set output "../figures/eval-analysis-postgres-512MB-ramdisk-log-size.eps"
plot "eval_analysis_postgres_512MB_ramdisk_log_size.dat" using 2:xtic(1) with lines ls 1 lc 1 lw 1 title columnheader(2),\
	"" using 3:xtic(1) with lines ls 2 lc 2 lw 2 title columnheader(3), \
	"" using 4:xtic(1) with lines ls 3 lc 3 lw 3 title columnheader(4), \
	"" using 5:xtic(1) with lines ls 4 lc 4 lw 4 title columnheader(5)
