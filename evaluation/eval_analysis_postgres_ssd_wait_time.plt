#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 25
#set size 1,1
#set key invert reverse left Left width -1
set key invert reverse outside top vertical width -1 height 1 samplen 2 font "Times-Roman, 20"
set ylabel 'Total wait time (s)' offset 2,0 
#set xrange [-0.3:2.3]
set yrange [0:]
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -45
set xtics ("ALL" 0, "SYNC" 1, "WAL" 2, "CP" 3, "CP+PCI" 4, "CP+PCI+IOCI" 5) font "Times-Roman, 22"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
set ytics 0,2000
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5

set output '../figures/eval-analysis-postgres-ssd-wait-time.eps'
plot 'eval_analysis_postgres_ssd_wait_time.dat' using 2 t 'wait\_on\_page\_writeback' lt 1 fs solid 0.10, '' using 3 t 'sleep\_on\_shadow\_bh'  lt 1 fs pattern 1, '' using 4 t 'lock\_bufer'  lt 1 fs solid 0.40, '' using 5 t 'mutex\_lock'  lt 1 fs pattern 4, '' using 6 t 'wait\_transaction\_locked' lt 1 fs solid 0.60, '' using 7 t 'down\_read' lt 1 fs pattern 7, '' using 8 t 'etc' lt 1 fs solid 0.80
