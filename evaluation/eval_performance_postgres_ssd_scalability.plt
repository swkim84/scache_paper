#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "Average TPC-C throughput (KtpmC)" offset 1.0,0
set xlabel "# of clients" #offset 1.5,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

set key top horizontal outside width 0 height 1 samplen 2 font "Times-Roman, 22"

set yrange [0:40]
set xrange [-0.5:5.5]
set xtics nomirror
set ytics nomirror
set xtics ('1' 0, '8' 1, '16' 2, '24' 3, '32' 4, '40' 5)
set grid y

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

#set size 1,0.5
set output "../figures/eval-performance-postgres-ssd-scalability.eps"
plot "eval_performance_postgres_ssd_16GB_scalability.dat" using 1:($2/1000):($6/1000) with yerrorlines lw 2 lt 4 pt 1 ps 1.5 title 'ALL', '' using 1:($3/1000):($7/1000) with yerrorlines lw 2 lt 2 pt 4 ps 1.5 title 'SYNC', '' using 1:($4/1000):($8/1000) with yerrorlines lw 2 lt 3 pt 7 ps 1.5 title 'WAL', '' using 1:($5/1000):($9/1000) with yerrorlines lw 2 lt 1 pt 9 ps 1.5 title 'CP+PCI+IOCI'
