\section{Which Type of Write is Critical?}\label{sec:DesignIssue}

%This section describes the rationale behind our definition of critical write. We then explain challenges for accurately identifying critical writes.

\subsection{Request-Oriented Write Classification}

The primary role of a storage application is to provide a specific storage service in response to an external \textit{request}, like a SET to a key-value store. Consequently, the performance of request processing determines the level of application performance that application users perceive. Therefore, we need to consider which type of writes delays the progress of request processing to classify critical writes. 

Synchronous writes can be a good candidate for the type of critical writes. Traditionally, write I/O is broadly classified into two categories in the system's viewpoint: asynchronous and synchronous. When a process issues an asynchronous write, it can immediately continue other processing jobs without waiting for the completion of the write. A synchronous write, on the other hand, is awaited by a requesting process until the write completes. Due to this difference, prioritizing synchronous writes over asynchronous ones is known as a reasonable method to reduce system-wide I/O wait time~\cite{Ganger1993}, and hence it is adopted in commodity OS~\cite{Corbet2009}. 

However, not all synchronous writes are truly synchronous from the perspective of \textit{request execution}. Typical storage applications delegate a large amount of synchronous writes to a set of background processes as a way of carrying out internal activities. For instance, storage applications such as RDBMS~\cite{Gray1992,Malviya2014}, NoSQL store~\cite{RedisPersistence,DeCandia2007}, and file system~\cite{Prabhakaran2005,Rosenblum1991} adopt a variant of logging technique that accompanies only a small amount of (mostly sequential) synchronous writes during request processing while conducting a burst of (mostly random) synchronous writes in background. This is an intrinsic design to segregate costly synchronous writes from the critical path of request execution as much as possible for achieving high degree of application performance without loss of durability. 

\begin{table}[t]
\begin{center}
%\begin{footnotesize}
\begin{tabular}{|c||c|r|}
\hline
\textbf{Write Type} & \textbf{Process} & \textbf{Ratio (\%)} \\
\hline \hline
\multirow{5}{*}{\textbf{Sync.}}
        & backends &  44.312 \\
    \cline{2-3}
            & checkpointer & 34.664 \\
        \cline{2-3}
        & log writer & 0.368 \\
    \cline{2-3}
            & jbd2 (kernel) & 0.094 \\
        \cline{2-3}
        & etc & 0.007 \\
    \hline

    \multirow{2}{*}{\textbf{Async.} }
            & kworker (kernel) & 20.554 \\
        \cline{2-3}
        & etc & 0.002 \\
    \cline{2-3}
\hline
\end{tabular}
%\end{footnotesize}
\end{center}
\caption{\textbf{Breakdown of writes by type and process.} \textit{}}
\label{tab:tpcc-io-breakdown}
\end{table}

To validate such behaviors, we ran the TPC-C benchmark using 24 clients without NVWC and recorded the type of write issued per process. As shown in Table~\ref{tab:tpcc-io-breakdown}, about 80\% of the writes are synchronous, and most of them are performed by \textit{backends} and \textit{checkpointer}. In PostgreSQL, the backend is a dedicated process for handling requests while the checkpointer periodically issues a burst of synchronous writes to reflect buffer modifications to a backing storage. Likewise, in kernel-level, journaling daemon (i.e., \textit{jbd2}) also issues synchronous writes (though small amount in this case) for committing and checkpointing file system transactions. Basically, the synchronous writes requested by the processes other than the backends are irrelevant to request processing. Furthermore, according to our analysis result (Table~\ref{tab:tpcc-cached-data-breakdown}), asynchronous writes occasionally block the backends because of complex synchronizations during runtime. The conventional synchrony-based classification, therefore, is inadequate for classifying critical writes.

We introduce \textit{request-oriented write classification} that classifies a write awaited in the context of request execution as a critical write regardless of whether it was issued synchronously or not. Based on this classification, only critical writes are cached into NVWC so that the request is handled quickly while non-critical writes are routed to backing storage directly. 
%To the best of our knowledge, this is the first attempt to classify write I/Os based on the relation of request execution.

The main benefits of the proposed classification are threefold. First, unnecessary stalled writes in a critical path can be avoided as much as possible by directly routing a burst of non-critical writes to a backing storage. Second, severe queue congestion in the request queues of NVWC incurred by bandwidth-hungry background I/O jobs can be largely relaxed, thereby effectively reducing queueing delay of critical writes. Third, device-specific reliability and performance issues can be eased. For example, in the cases of flash- and PCM-based NVWC, the lifetime of the NVWC can be extended by reducing the number of cached writes without hurting application performance.

\subsection{Dependency-Induced Critical Write}\label{sec:DesignIssueDependency}

In many storage applications, one or more processes are dedicated to handle requests. Synchronous writes issued by these processes are definitely critical; hence, we refer to this type of processes as a \textit{critical process}. Caching synchronous writes requested by critical processes alone, however, is insufficient for identifying all critical writes because of runtime dependencies generated by complex synchronizations among concurrent processes and I/Os. 

There are two types of dependencies associated with write I/Os: a process dependency and an I/O dependency. The process dependency occurs when two processes interact with each other via synchronization primitives such as a lock, barrier, and condition variable. The process dependency complicates the accurate detection of critical writes because the non-critical process may issue synchronous writes within a critical section making the critical process indirectly wait for the completion of the writes. On the other hand, the I/O dependency occurs between a critical process and an on-going write I/O. Basically, the I/O dependency is generated when a critical process needs to directly wait for the completion of an outstanding write in order to ensure consistency and/or durability. 

\begin{figure}[t]
\centering
\epsfig{file=figures/design-analysis-dependency.eps, width=4.5in}
\caption{\textbf{Impact of dependencies on application performance.} \textit{CP caches synchronous writes requested by critical processes while Optimal caches all writes without stalls. Network latencies are omitted for brevity.}}
\label{fig:design-analysis-dependency}
\end{figure}

In order to quantify the significance of the dependency problems, we measured the wait time of critical processes (i.e., PostgreSQL backends) using \textit{LatencyTOP}~\cite{Edge2008} during the execution of the TPC-C benchmark using 24 clients with 4GB ramdisk-based NVWC. Figure~\ref{fig:design-analysis-dependency} shows the impact of complex dependencies on the TPC-C throughput; CP caches only synchronous writes requested by critical processes while Optimal caches all writes without stalls. As we expect, CP mostly eliminates the latency incurred by synchronous writes (i.e., \texttt{wait\_on\_page\_writeback()}). However, CP still suffers from excessive latencies mainly caused by process dependency (i.e., \texttt{mutex\_lock()}) and I/O dependency (i.e., \texttt{sleep\_on\_shadow\_bh()}). Note that the I/O dependency occurs because a critical process attempts to update a buffer page that is under writing back as a part of a committing file system transaction. Consequently, CP achieved only a half of the best possible performance improvement (Optimal).
%\sw{Note that the sources of dependency changes in CP because backends now proceed quickly without long waiting for synchronous writes while the write I/Os requested by the other processes processed slowly.}

\begin{table}[t]
\begin{center}
%\begin{footnotesize}
\begin{tabular}{|c||c|r|r|}
\hline
\textbf{Dep.} & \textbf{Synchronization} & \textbf{Avg} & \textbf{Max} \\
\textbf{Type} & \textbf{Method} & \textbf{(ms)} & \textbf{(ms)} \\
\hline \hline
\multirow{5}{*}{\textbf{Process}}
        & \texttt{down\_read} & 1088.09 & 6065.2 \\
    \cline{2-4}
            & \texttt{wait\_transaction\_locked} & 493.05 & 4806.8 \\
        \cline{2-4}
	& \texttt{mutex\_lock} & 134.55	& 6313.55 \\
    \cline{2-4}
            & \texttt{jbd2\_log\_wait\_commit} & 40.96 & 391.36 \\
    \hline

    \multirow{4}{*}{\textbf{I/O} }
            & \texttt{lock\_buffer} & 912.38 & 3811.35 \\
        \cline{2-4}
        & \texttt{sleep\_on\_shadow\_bh} & 225.25 & 3560.47 \\
    \cline{2-4}
            & \texttt{lock\_page} & 8.08 & 3009.84 \\
        \cline{2-4}
        & \texttt{wait\_on\_page\_writeback} & 0.04 & 19.12 \\
    \cline{2-4}
\hline
\end{tabular}
%\end{footnotesize}
\end{center}
\caption{\textbf{Sources of dependencies.} \textit{Average and maximum wait times of backends are shown in the CP case.}}
\label{tab:tpcc-cp-latency}
\end{table}

In addition, there are many other sources of excessive latencies in terms of the average and worst case as shown in Table~\ref{tab:tpcc-cp-latency}. The read/write semaphore for serializing on-disk inode modifications represented as \texttt{down\_read()} induces about one second and several seconds latencies in the average and worst case, respectively. The journaling-related synchronizations for ensuring file system consistency also incur high latencies. In particular, \texttt{wait\_transaction\_locked()} is called to synchronize with all the processes updating the current file system transaction to complete their execution while \texttt{jbd2\_log\_wait\_commit()} is called to wait for the journaling daemon to complete the commit procedure of a file system transaction. The synchronization methods that induce I/O dependency such as \texttt{lock\_buffer()} and \texttt{lock\_page()} delays the progress of critical processes up to several seconds. Though some of the synchronization methods account for a small portion of the total latency, they would increase tail latency, thereby degrading user experience in large-scale services~\cite{Dean2013}. Therefore, all synchronization methods causing latency to the critical processes should be handled properly in order to eliminate unexpected request latency spikes.
%\sw{Interaction arisen from reading in new buffer to DB buffer pool. This is excerbated by speed gap between consuming DB buffer pool and cleaning.}

\section{Critical Write Detection}\label{sec:DesignSolution}

%This section presents how the proposed scheme accurately detects all critical writes. We first illustrate our approach for distinguishing critical processes at kernel-level. Then, we explain how to resolve dynamic dependencies associated with critical processes.

\subsection{Critical Process Identification}

%There are two possible approaches for identifying critical processes: an OS-level approach and an application-guided approach. The OS-level approach is based on monitoring and tracking inter-process communication (IPC) at the kernel-level to infer a set of critical processes, similar to the previous approaches~\cite{Zheng2004,Zheng2010}. The OS-level approach typically requires lots of instrumentation to various in-kernel methods to cover all kinds of IPC and some heuristics (e.g., feedback-based confidence evaluation~\cite{Zheng2004}) to reduce the possibility of misidentification. On the other hand, the application-guided approach can precisely identify a set of critical processes by directly exploiting application-level semantics. The main drawback of the application-guided approach is that it requires application modifications. 

%In this work, we adopt the application-guided approach as our mechanism for critical process identification for the following reasons. First, it can precisely identify all the critical processes in the system with low engineering cost, and hence less error-prone. Second, it is easy to implement and portable since the hint revealing a critical process is the type of a disclosing hint~\cite{Patterson1995} that does not require any specific knowledge about underlying systems. Third, it requires adding only few lines of code to an application in practice because typical storage applications (e.g., MySQL~\cite{MySQL}) already distinguishes foreground processes from background processes; see Section~\ref{sec:Applications} for our application studies.  

In order to detect all critical writes, we should identify a set of critical processes in the first place. To do so, we adopt an application-guided approach that exploits application-level hints for identifying critical processes. 

The main benefit of the application-guided approach is that it can precisely determine critical processes without modifications of the OS kernel. Accurately identifying critical processes without application guidance requires huge engineering effort to the kernel. For instance, similar to the previous approaches~\cite{Zheng2010,Zheng2004}, the kernel should track all inter-process communication and network-related I/Os to infer the processes handling requests. In addition, the kernel should adopt complex heuristics (e.g., feedback-based confidence evaluation~\cite{Zheng2004}) to reduce the possibility of misidentification. On the other hand, an application can accurately decide criticality of each process since the application knows the best which processes handle requests.

Though the application-guided approach requires application modifications, the engineering cost for modifying an application is low. This is because an application developer does not need to know the specifics of underlying systems since the hint revealing a critical process is a disclosing hint~\cite{Patterson1995}. In addition, typical storage applications such as MySQL~\cite{MySQL}, PostgreSQL, and Redis already distinguish foreground processes (i.e., critical processes) from background processes. Therefore, the required modification is only a few lines of code in practice; see Section~\ref{sec:Applications} for our application studies.

Based on the critical process identification, synchronous writes requested by critical processes are cached in NVWC. Since a hint is solely used for deciding admission to NVWC, a wrong hint does not affect the correct execution of the application. However, hint abuse by a malicious or a thoughtless application may compromise performance isolation among multiple applications sharing NVWC. Addressing the fairness issue resulting from sharing NVWC is out of scope of this chapter.

\begin{figure*}[t]
\centering
\subfigure[Process criticality inheritance\label{fig:design-criticality-inheritance-process}]{\epsfig{file=figures/design-criticality-inheritance-process.eps, width=2.9in}}
\subfigure[I/O criticality inheritance\label{fig:design-criticality-inheritance-io}]{\epsfig{file=figures/design-criticality-inheritance-io.eps, width=2.9in}}
\subfigure[Handling cascading dependencies\label{fig:design-blocking-status-tracking}]{\epsfig{file=figures/design-blocking-status-tracking.eps, width=2.9in}}
\caption{\textbf{Criticality inheritance protocols.} \textit{Thick lines represent a critical path of request execution while dotted lines indicate blocked execution. Circles and boxes represent write I/Os to specific blocks and blocking objects of specific processes, respectively. Thick arrows indicate specific actions described in the corresponding texts while thin arrows to/from write I/Os show I/O submission/completion.}}
\label{fig:design-criticality-inheritance}
\end{figure*}

\subsection{Criticality Inheritance Protocols}

As we discussed in Section~\ref{sec:DesignIssueDependency}, the process and I/O dependencies can significantly delay the progress of a critical process. In the rest of this section, we explain our \textit{criticality inheritance protocols} that effectively handle the process and I/O dependencies.

\subsubsection{Process Criticality Inheritance}

Handling the process dependency has been well-studied in the context of process scheduling because the process dependency may cause priority inversion problem~\cite{Lampson1980}. Priority inheritance~\cite{Sha1990} is the well-known solution for resolving priority inversion caused by the process dependency. 
%Later, automatic detection of IPC-induced process dependencies also has been investigated for effective process scheduling~\cite{Zheng2004}.

Inspired by the previous work, we introduce \textit{process criticality inheritance} to resolve the process dependency. Process criticality inheritance is similar to the priority inheritance in that a non-critical process inherits criticality when it blocks a critical process until it finishes its execution within the synchronized region. The main difference between process criticality inheritance and priority inheritance is that the former is used to prioritize I/Os whereas the latter is used to prioritize processes.

Figure~\ref{fig:design-criticality-inheritance-process} illustrates an example of process criticality inheritance: (1) critical process P1 attempts to acquire a lock to enter a critical section. (2) Non-critical process P2 inherits criticality from P1 since the lock is held by P2. Then, the synchronous write to block B1 issued by P2 is directed to NVWC to accelerate the write within the critical path. (3) P2 wakes up P1 when its execution within the critical section has been completed, and P1 continues the rest of its job. In this example, the write latency of block B1 is minimized by using process criticality inheritance since it indirectly delays the request execution.

\subsubsection{I/O Criticality Inheritance}

Handling the I/O dependency is more complicated than that of the process dependency since inheriting criticality to the on-going write to a backing storage requires reissuing the write to NVWC without side effects (e.g., duplicated I/O completion). A possible solution for eliminating the side effects is canceling an outstanding write to a disk. In practice, however, canceling a specific on-going write needs significant engineering efforts due to multiple abstraction stages in I/O stack. In Linux, for example, an I/O can be staged in either an OS queue managed by an I/O scheduler or a storage queue managed by a device firmware. Hence, the procedure of canceling requires lots of modifications to various in-kernel components.

In order to rapidly resolve the I/O dependency while maintaining low engineering cost, we devise immediate reissuing and I/O completion discarding as a technique for I/O criticality inheritance. Figure~\ref{fig:design-criticality-inheritance-io} describes the proposed mechanism: (1) critical process P1 needs to wait for the completion of the write request to block B2. (2) P1 reissues B2 to NVWC to resolve the I/O dependency between P1 and B2. (3) The event of I/O completion on the reissued B2 wakes up P1. (4) Later, the event of the I/O completion on the original write to B2 is discarded to suppress the duplicated completion notification. 

The main drawback of the proposed technique for I/O criticality inheritance is that it cannot eliminate unnecessary write traffic to a disk since it does not cancel on-going writes to the disk. However, the performance penalty would be small since the duplicated write to a specific block is highly likely to be processed as a single sequential write merged with other writes to adjacent blocks in modern OSes. Moreover, discarding several blocks included in a write request may result in splitting the request into multiple requests, thereby decreasing the efficiency of I/O processing.

\subsubsection{Handling Cascading Dependencies}

Cascading dependencies, a chain of process and I/O dependencies, make precise detection of critical writes more difficult if the chain contains an already-blocked process. For example, as illustrated in Figure~\ref{fig:design-blocking-status-tracking}, non-critical process P3 issues a synchronous write and is blocked to wait for the completion of the write. Later, non-critical process P2 sleeps while holding a lock because it needs to wait for an event from P3. In this situation, if critical process P1 attempts to acquire the lock that is held by P2, P1 blocks until the write issued by P3 is completed even though P2 inherits criticality from P1. We found that this scenario occurs in practice because of complex synchronization behaviors for ensuring file system consistency.

In order to handle the cascading dependencies, we record a blocking object to the descriptor of a process when the process is about to be blocked. There are two types of the blocking object in general: process and I/O. A process or I/O is recorded as a blocking object when another process needs to sleep waiting for either the event from the process or the completion of the I/O. As a special case, a lock is recorded as the blocking object when a process should sleep to acquire the lock, in order to properly handle the cascading dependencies to both the lock owner and waiters having higher lock-acquisition priority. Based on the recorded blocking object, a critical process can effectively track the cascading dependencies and can handle them using the process and I/O criticality inheritances.

Figure~\ref{fig:design-blocking-status-tracking} demonstrates an example to describe how the cascading dependencies are handled: (1) critical process P1 attempts to acquire the lock that is held by non-critical process P2. (2) Then, P2 inherits criticality from P1, and P1 checks P2's blocking object. (3) Since P2 is currently blocked waiting for an event from non-critical process P3, P3 also inherits criticality and P1 again checks P3's blocking object. (4) P1 initiates reissuing of block B1 to NVWC because P3 currently blocks on B1, and sleeps until the lock has been released by P2. (5) The I/O completion of the reissued B1 wakes up P3, and (6) P3 wakes up P2 after doing some residual work. (7) P2, in turn, wakes up P1 after completing its execution in the critical section. Finally, P1 enters the critical section and continues its job. 
