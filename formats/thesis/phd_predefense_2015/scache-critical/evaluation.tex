\section{Evaluation}\label{sec:Evaluation}

This section presents evaluation results based on the prototype implementation. We first detail the experimental environment. Then, we show the experimental results for both PostgreSQL and Redis to validate the effectiveness of the proposed scheme. 

\subsection{Experimental Setup}\label{sec:EvaluationSetup}

Our prototype was installed on Dell PowerEdge R420, equipped with two quad-core Intel Xeon E5-2407 2.4GHz processors and 16GB RAM; CPU clock frequency is set to the highest level for stable performance measurement. The storage subsystem is comprised of three 500GB 10K RPM WD VelociRaptor HDDs, one of which is dedicated to OS and the others are used as backing storage of NVWC. We used Ubuntu 14.04 with the modified Linux kernel version 3.12 as an OS and ext4 mounted with the default options as a file system.

For NVWC devices, we allocated 4GB region of 16GB RAM to ramdisk and used a 256GB Samsung 840 Pro SSD. To correctly emulate the persistency of the ramdisk-based NVWC in the existence of volatile CPU caches, we used non-temporal memory copy described in~\cite{Chen2014} when the data is written to ramdisk. For the stable performance measurement of the SSD-based NVWC, we discard all the blocks in SSD and give enough idle time before starting each experiment. In addition, write caching to on-board cache of the storage devices, including the HDDs and the SSD, was turned off to eliminate performance variations caused by internal buffering.

We used two criticality-oblivious admission policies: ALL and SYNC. ALL, which is the FlashCache default, caches all incoming writes while SYNC caches only synchronous writes. In addition, we used three criticality-aware admission policies: CP, CP+PI, and CP+PI+IOI. CP caches synchronous writes requested by critical processes. CP+PI caches process dependency-induced critical writes in addition to CP. CP+PI+IOI additionally caches I/O dependency-induced critical writes.

\begin{figure*}[t]
\centering
\subfigure[Ramdisk NVWC\label{fig:eval-performance-postgres-ramdisk}]{\epsfig{file=figures/eval-performance-postgres-ramdisk-capacity.eps, width=3.5in}}
\subfigure[SSD NVWC\label{fig:eval-performance-postgres-ssd}]{\epsfig{file=figures/eval-performance-postgres-ssd-capacity.eps, width=3.5in}}
\caption{\textbf{PostgreSQL performance.} \textit{TPC-C throughput is averaged over three runs for each admission policy.}}
\label{fig:eval-performance-postgres}
\end{figure*}

\subsection{PostgreSQL with TPC-C}\label{sec:EvaluationPostgreSQL}

We used TPC-C~\cite{TPCC} as the realistic workload for PostgreSQL. We set TPC-C scale factor to ten, which corresponds to about 1GB of initial database, and simulated 24 clients running on a separate machine for 30 minutes. We report the number of New-Order transactions executed per minute (i.e., tpmC) as the performance metric. PostgreSQL was configured to have 512MB buffer pool, and the size of log files triggering checkpointing was set to 256MB. The database and log files are located on different HDDs according to the recommendation in the official document~\cite{PostgresStorageSetup}. As the practical alternative of selective caching~\cite{Chen2009b,Copeland1989,Heiser2013,Lee2008}, we used an additional policy denoted as WAL that caches all the write traffic to the log disk to NVWC. Since our work focuses on caching write I/Os, we eliminate read I/Os by warming up the OS buffer cache before starting the benchmark.

\textbf{Performance with ramdisk.} Figure~\ref{fig:eval-performance-postgres-ramdisk} shows the TPC-C throughput averaged over three runs as the ramdisk-based NVWC capacity increases from 32MB (scarce) to 4GB (sufficient). ALL achieves the lowest performance in the 32MB case because it stalls 58\% of all writes. ALL gradually improves the performance as the NVWC capacity increases due to the reduction of write stalls. SYNC slightly improves the performance compared to ALL in the low capacities since it reduces the number of write stalls by filtering out asynchronous writes. SYNC, however, cannot catch up the performance of ALL in the high capacities since it suffers from the dependencies induced by the asynchronous writes. Though WAL and CP do not suffer from write stalls at all in all the capacities, they achieve still lower performance than CP+PI and CP+PI+IOI due to the complex dependencies. CP+PI further improves performance by 4--12\% over CP by handling process dependencies. CP+PI+IOI outperforms CP+PI by 18--29\% by additionally handling I/O dependencies. Compared to ALL, CP+PI+IOI gains 80\% performance improvement in the 32MB case and 72\% reduction of cached writes without performance loss in the 4GB case.

\begin{figure}[t]
\centering
\epsfig{file=figures/eval-analysis-postgres-wait-time-512MB.eps, width=3.5in}
\caption{\textbf{Breakdown of PostgreSQL backends latency.} \textit{512MB ramdisk is used as the NVWC device and network latencies are omitted for brevity.}}
\label{fig:eval-analysis-postgres-ramdisk-dependency}
\end{figure}

To further analyze the reason behind the performance differences, we measured the wait time of critical processes (i.e., PostgreSQL backends) in the 512MB NVWC case. As shown in Figure~\ref{fig:eval-analysis-postgres-ramdisk-dependency}, ALL and SYNC incur the synchronous write latency (i.e., \texttt{wait\_on\_page\_writeback}) and the mutex- and file system journaling-induced latencies (i.e., \texttt{sleep\_on\_shadow\_bh} and \texttt{wait\_transaction\_locked}) described in Section~\ref{sec:DesignIssue}, mainly due to frequent write stalls. Though WAL and CP mostly eliminate the synchronous write latencies by eliminating write stalls, they still incur excessive latencies mainly caused by the mutex and file system journaling. Though CP+PI further reduces latencies by resolving the mutex-induced dependency, it delays the progress of the critical processes because of unresolved I/O dependences. CP+PI+IOI eliminates most of the latencies since it additionally resolves I/O dependencies including the dependency to the journaling writes. As a result, CP+PI+IOI achieves the highest level of application performance in all the capacities.

\textbf{Performance with SSD.} Figure~\ref{fig:eval-performance-postgres-ssd} shows the TPC-C throughput averaged over three runs as the SSD-based NVWC capacity increases from 4GB to 128GB; the concurrent I/Os for writeback decreases as the NVWC capacity increases. Unlike the case of the ramdisk-based NVWC, ALL achieves lower performance than the criticality-aware policies in all the capacities due to severe congestion in the request queues of the SSD. Though SYNC can ease the contention in the SSD more than ALL, it still has dependency problems incurred by the asynchronous writes directly routed to HDDs. On the other hand, WAL and the criticality-aware policies improve the application performance due to less congestion compared to both ALL and SYNC. In particular, CP+PI+IOI outperforms ALL and SYNC by about 1.8--2.2$\times$ and 2.2--2.5$\times$, respectively, because it minimizes the queueing delays of critical writes by filtering out over a half of writes while effectively handling process and I/O dependencies.
%Why starting from 4GB and performance is improved as the capacity increaes.

\begin{figure}[t]
\centering
\epsfig{file=figures/eval-analysis-postgres-ssd-iops.eps, width=3.5in}
\caption{\textbf{Impact of queueing delay.} \textit{16GB SSD is used as the NVWC device and synchronous write latency of critical processes is represented as NVWC write latency.}}
\label{fig:eval-analysis-postgres-ssd-iops}
\end{figure}

To show the impact of queueing delay on critical writes, we measured the 4KB IOPS for both the SSD and HDDs, and the average latency of synchronous writes requested by backends.  As shown in Figure~\ref{fig:eval-analysis-postgres-ssd-iops}, ALL and SYNC utilize the SSD better than the other policies. This high utilization, however, causes severe congestion in the request queues of SSD, thereby delaying the processing of critical writes. WAL and the criticality-aware policies utilize both the SSD and the HDDs in a more balanced manner, thereby decreasing the queueing delay of critical writes. 

\begin{figure}[t]
\centering
\epsfig{file=figures/eval-analysis-postgres-ramdisk-efficiency.eps, width=3.5in}
\caption{\textbf{Caching efficiency.} \textit{Ramdisk is used as the NVWC device.}}
\label{fig:eval-analysis-postgres-ramdisk-efficiency}
\end{figure}

\textbf{Caching efficiency.} In order to quantify the caching efficiency in terms of application performance, Figure~\ref{fig:eval-analysis-postgres-ramdisk-efficiency} plots the performance per cached block as the capacity of the ramdisk-based NVWC increases. WAL and the criticality-aware policies show higher caching efficiencies compared to ALL and SYNC. Note that ALL and SYNC show high caching efficiency in the low NVWC capacities because FlashCache directs a write I/O to a backing storage instead of waiting for a free block when there is no free block in NVWC. Overall, CP+PI+IOI utilized NVWC more efficiently by 1.2--3.7$\times$ and 1.2--2.2$\times$ compared to ALL and SYNC, respectively. 
%This result implies that our scheme can extend the lifetime of an NVWC device that has limited write endurance without sacrificing application performance.

\begin{table}[t]
\begin{center}
%\begin{footnotesize}
	\centering
	\begin{tabular}{|c|c||r|r|r|r|}
		\cline{2-6}
		\multicolumn{1}{r|}{} & \textbf{Ratio (\%)} & \textbf{CP} & \textbf{PI} & \textbf{IOI} & \textbf{Total} \\
		\hline
		\multirow{5}{*}{\textbf{Data}} & Data (DB) & 0 & 0.5209 & 0.0515 & 0.5723 \\ 
		& Data (LOG) & 98.7409 & 0.5707 & 0.0007 & 99.3123 \\ 
		& Metadata & 0 & 0 & 0.0014 & 0.0014 \\ 
		& Journal & 0 & 0.0782 & 0.0357 & 0.1140 \\ 
		\cline{2-6}
		& Total & 98.7409 & 1.1698 & 0.0893 & 100 \\
		\hline
		\multirow{3}{*}{\textbf{I/O}} & Sync. & 98.7409 & 1.0312 & 0.0357 & 99.8078 \\ 
		& Async. & 0 & 0.1387 & 0.0535 & 0.1922 \\ 
		\cline{2-6}
		& Total & 98.7409 & 1.1698 & 0.0893 & 100 \\
		\hline
	\end{tabular}
%\end{footnotesize}
\end{center}
\caption{\textbf{Breakdown of critical writes.} \textit{4GB ramdisk is used as the NVWC device in the case of CP+PI+IOI.}}
\label{tab:tpcc-cached-data-breakdown}
\end{table}

\textbf{Breakdown of critical writes.} To help understand which types of data and I/O constitute critical writes, Table~\ref{tab:tpcc-cached-data-breakdown} shows the breakdown of critical writes in terms of data and I/O types. As we expect, the dominant type of data comprising critical writes is the logs that are synchronously written by backends during transaction commits. However, the rest of the critical writes is still crucial since it contributed to additional 32\% performance improvement over CP alone (Figure~\ref{fig:eval-performance-postgres-ramdisk}). On the other side, the dominant type of I/O comprising critical writes is synchronous writes. Though the portion of asynchronous writes is insignificant, it contributed to additional 38\% performance improvement over SYNC (Figure~\ref{fig:eval-performance-postgres-ramdisk}). Overall, dependency-induced critical writes have significant impact on application performance.
% WAL makes criticai processes suffer from dependencies caused by data files. 
% We believe our scheme excels when flash SSD is fully utilized because we mostly caches log files that has the same lifecycle and sequential pattern, hence more robust performance in the existence of intensive garbage collection. The mostly sequential pattern of cached write also improves writeback performance to disk-based backing storage.

\begin{figure}[t]
\centering
\epsfig{file=figures/eval-analysis-postgres-ramdisk-iops.eps, width=3.5in}
\caption{\textbf{Performance disparity.} \textit{512MB ramdisk is used as the NVWC device.}}
\label{fig:eval-analysis-postgres-ramdisk-iops}
\end{figure}

\textbf{Performance disparity.} Interestingly, we found the disparity between the system performance (i.e., IOPS) and the application performance (i.e., tpmC). For instance, as shown in Figure~\ref{fig:eval-analysis-postgres-ramdisk-iops}, ALL better utilizes NVWC by 40\% than CP+PI+IOI leading to achieve 10\% higher system performance in the 512MB ramdisk case. However, CP+PI+IOI accomplishes 57\% higher application performance than that of ALL because CP+PI+IOI avoids write stalls in the critical paths. This result validates our argument on the necessity of the request-oriented write classification in order to effectively utilize a given NVWC device.
%This finding is inconsistent with the widely held expectation that high system performance result in high application performance. Therefore, application performance should be centered for designing a system since the system's reason for being is satisfying application, and in turn application users, not the system itself.

\subsection{Redis with YCSB}\label{sec:EvaluationRedis}

For Redis, we used the update-heavy (Workload A) and read-mostly (Workload B) workloads provided by the YCSB benchmark suit~\cite{Cooper2010}. The data set was composed of 0.5 million objects each of which is 1KB in size. We simulated 40 clients running on a separate machine to generate ten millions of operations in total. We report operations per second (i.e., ops/s) as the performance metric. For Redis, we enabled both snapshotting and command logging according to the suggestion in the official document~\cite{RedisPersistence}. Due to the single threaded design of Redis~\cite{RedisLatency}, we concurrently ran four YCSB benchmarks against four Redis instances to utilize our multi-core testbed.

\begin{figure}[t]
\centering
\epsfig{file=figures/eval-performance-redis.eps, width=3.5in}
\caption{\textbf{Redis performance.} \textit{512MB ramdisk and 16GB SSD are used as the NVWC devices.}}
\label{fig:eval-performance-redis}
\end{figure}

\textbf{Performance.} Figure~\ref{fig:eval-performance-redis} demonstrates the average YCSB throughput over three runs normalized to ALL. SYNC improves the performance over ALL since it filters out the asynchronous writes issued by the kernel thread that cleans the OS buffer cache. Unlike the case of PostgreSQL, CP shows significantly low performance compared to the other policies because Redis frequently incurs synchronous writes conducted by a journaling daemon, which cannot be detected as the critical process by CP, within the critical path of update request. CP+PI dramatically improves the performance by 2--30$\times$ over CP because it additionally caches the journaling writes when there is a dependency between a critical process and the journaling daemon. CP+PI+IOI further improves the performance by 3--49\% over CP+PI by additionally resolving the I/O dependencies incurred by the synchronizations to guarantee the file system consistency. By providing the first class support for critical writes, CP+PI+IOI outperforms ALL by 17--32\% and 47--55\% in the ramdisk- and SSD-based NVWC, respectively. 
%\sw{Comment on reduced write traffic is required since the value of the reduced ratio is mentioned in both abstract and introduction.}

\begin{table}[t]
\begin{center}
%\begin{footnotesize}
	\centering
	\begin{tabular}{|c|c||r|r|r|}
		\cline{2-5}
		\multicolumn{1}{r|}{} & \textbf{Latency (ms)} & \textbf{99th-\%ile} & \textbf{99.9th-\%ile} & \textbf{99.99th-\%ile} \\
		\hline
		\multirow{3}{*}{\textbf{A}} & ALL & 80 & 649 & $>$1000 \\
		& SYNC & 72 & 678 & $>$1000 \\
		& CP+PI+IOI & 32 & 50 & 79 \\ 
		\hline
		\multirow{3}{*}{\textbf{B}} & ALL & 70 & 572 & $>$1000 \\
		& SYNC & 59 & 438 & $>$1000 \\
		& CP+PI+IOI & 23 & 32 & 83 \\ 
		\hline
	\end{tabular}
%\end{footnotesize}
\end{center}
\caption{\textbf{Redis tail latency.} \textit{16GB SSD is used as the NVWC device.}}
\label{tab:ycsb-tail-latency}
\end{table}

\textbf{Tail latency.} To show the impact of the admission policies on tail latency, we present the latency distribution of YCSB requests in the SSD-based NVWC case. As shown in Table~\ref{tab:ycsb-tail-latency}, only CP+PI+IOI keeps $99.9^{th}$ and $99.99^{th}$-percentile latencies below 100ms, which makes users feel more responsive than higher latencies~\cite{Card1991}. ALL and SYNC, on the other hand, increase the $99.9^{th}$-percentile latency by an order of magnitude compared to that of CP+PI+IOI. Moreover, the $99.99^{th}$-percentile latencies of ALL and SYNC exceeds one second, which is the maximum latency reported by YCSB. Though Workload B mostly consists of read request, the tail latency is largely affected by the admission policy used. This is because Redis serves all requests sequentially using a single thread, thereby delaying the processing of read requests that are queued behind update requests. Considering the significance of providing consistent responses to users~\cite{Brutlag2009,Schurman2009} especially for large-scale services~\cite{Dean2013,Timothy2014}, this result indicates that the proposed scheme is essential for providing high quality services to users.

%We used TPC-C~\cite{TPCC} and YCSB~\cite{Cooper2010} as the realistic workloads for PostgreSQL and Redis, respectively:

%\begin{itemize}
%	\item \textbf{TPC-C} is a standard online transaction processing (OLTP) system benchmark simulating an order-entry environment. We used TPC-C scale factor ten, which corresponds to about 1GB of initial database, to make database fitting into main memory so that performance impact of read I/Os can be excluded. We simulated 24 clients for 30 minutes in all runs. We report the number of New-Order transactions executed per minute (i.e., tpmC) as the performance metric for TPC-C. 
%	\item \textbf{YCSB} is an industry-standard performance benchmark for NoSQL databases. We used the update-heavy (Workload A) and read-mostly (Workload B) workloads pre-configured in the benchmark suite. The data set was composed of 0.5 million objects each of which is 1KB in size. We simulated 40 clients to generate ten millions of operations in total, and we report operations per second (i.e., ops/s) as the performance metric for YCSB. 
%\end{itemize}

%For both benchmarks, the clients was run on a separate machine with a system configuration identical to that of the server connected directly via 1Gbps Ethernet port. In addition, we report performance (e.g., tpmC) per cached block for comparing caching efficiency. 

