\begin{figure*}[t]
\centering
\subfigure[Stalled writes (ramdisk)\label{fig:motiv-stalled-writes}]{\epsfig{file=../../figures/motiv-stalled-writes.eps, width=2.1in}}
\subfigure[Queueing delay (64GB SSD)\label{fig:motiv-queueing-delay}]{\epsfig{file=../../figures/motiv-queueing-delay.eps, width=2.1in}}
\subfigure[Concurrent I/Os for writeback (SSD)\label{fig:motiv-writeback-traffic}]{\epsfig{file=../../figures/motiv-writeback-traffic.eps, width=2.1in}}
\vspace{-0.1in}
\caption{\textbf{Limitations of caching all writes.} \textit{TPC-C throughput is normalized to the case of No NVWC.}}
\vspace{-0.2in}
\label{fig:motiv}
\end{figure*}

\section{Background and Motivation}\label{sec:BackgroundMotivation}

%This section explains the key characteristics of devices comprising NVWC and their use cases. We then argue why blindly caching all writes is inefficient in terms of application performance.

\subsection{Non-volatile Write Caches}\label{sec:Background}

Unlike conventional volatile caches, non-volatile write cache (NVWC) is mainly used to durably buffer write I/Os for improving write performance. Traditionally, NV-DRAM has been widely used as an NVWC device to enhance write performance by exploiting its low latency and persistency. Typical usages of NV-DRAM are writeback cache in RAID controllers~\cite{Gill2009,Gill2005} and drop-in replacement for DDR3 DIMMs~\cite{AgigaTech,VikingTech}. An inherent limitation of NV-DRAM is small capacity due to high cost per capacity and battery scaling problem.
%, which are equipped with a small size of NV-DRAM~\cite{Dell}
%NV-DRAM commonly exploits a battery (i.e., battery-backed DRAM) or flash memory with a super capacitor (i.e., flash-backed DRAM) to guarantee durability for cached data in DRAM. 

Recently, flash memory-based caching is gaining significant attention because it delivers much higher performance than traditional disks and much higher density than NV-DRAM. Thus, flash memory is widely adopted in many storage solutions, such as hybrid storage~\cite{Chen2011,Saxena2012} and client-side writeback caches in networked storage systems~\cite{Arteaga2014,Koller2013,Qin2014}. Despite of the benefits, flash memory also has caveats to be used as an NVWC device because it has limited write endurance~\cite{Grupp2009,Grupp2012,Yang2013} and garbage collection overheads~\cite{Huang2014,Kang2014B,Kgil2008,Oh2012}.
%The main benefit of flash memory is high density per cost as compared to that of NV-DRAM.

Emerging SCM, such as STT-MRAM~\cite{Burr2008} and PCM~\cite{Raoux2008}, is also a good candidate for an NVWC device since it provides low latency comparable to DRAM and persistency without backup power. Though STT-MRAM promises similar access latency to that of DRAM, its capacity is currently very limited due to technical limitation~\cite{Everspin}. On the other hand, PCM has been regarded as more promising technology to be deployed at commercial scale than STT-MRAM~\cite{Micron2012}. PCM, however, has limited write endurance~\cite{Lee2009,Qureshi2009}, which necessitates careful management when it is used as an NVWC device.
%Though STT-MRAM promises similar access latency to that of DRAM, it has low density per area due to technical limitation; the capacity of a currently available STT-MRAM device is only 64 Mb~\cite{Everspin}.

\subsection{Why Admission Policy Matters}\label{sec:Motivation}

A straightforward use of NVWC is to cache all writes and to writeback cached data to backing storage in a lazy manner. This simple admission policy is intended to provide low latency for all incoming writes as much as possible for improving \textit{system performance} (e.g., IOPS). However, blindly caching all writes cannot fully utilize the benefit of NVWC in terms of \textit{application performance} (e.g., transactions/sec) for the following reasons. 

Firstly, caching all writes can frequently stall writes that are in the critical paths of an application due to the lack of free blocks in NVWC. This is because the speed of making free blocks is eventually bounded by the writeback throughput to backing storage, such as disks. The stalled writes problem becomes more serious in capacity-constrained devices, such as NV-DRAM and STT-MRAM. In order to quantify the impact of stalled writes on application performance, we ran a TPC-C benchmark~\cite{TPCC} against PostgreSQL RDBMS~\cite{Postgres} on a Linux-based system having NV-DRAM-based NVWC (emulated via ramdisk); see Section~\ref{sec:Evaluation} for the detailed configuration. As shown in Figure~\ref{fig:motiv-stalled-writes}, the TPC-C throughput normalized to the case without NVWC (i.e., disk only) drops from 1.99 to 1.09 as the capacity of NVWC decreases. This is because the frequency of write stalls in critical paths of PostgreSQL (e.g., stalls during log commit) is highly likely to increase as the ratio of stalled writes increases. 

Secondly, caching all writes can incur significant congestion in OS- and device-level request queues of NVWC, thereby delaying the processing of critical writes. When the request queues of NVWC are congested, write requests need to wait at the queues even though the NVWC has sufficient free blocks. Moreover, queue congestion of a storage-based NVWC such as SSD can be exacerbated by concurrent I/Os for writing back cached data. The concurrent I/Os include NVWC reads for retrieving a dirty cache block into main memory and NVWC writes for updating the corresponding metadata. In order to measure the impact of aggravated queueing delay, we ran the TPC-C benchmark with a flash SSD-based NVWC. As shown in Figure~\ref{fig:motiv-queueing-delay}, the average length of OS request queue increases as the number of clients increases, thereby gradually degrading the normalized TPC-C throughput. In addition, the performance further decreases as the concurrent I/Os increase as shown in Figure~\ref{fig:motiv-writeback-traffic}; the frequency of writebacks depends on the ratio of dirty blocks in NVWC for this measurement. In most cases, NVWC provides even lower performance than that without NVWC (up to 47\% performance loss) though write stalls did not occur at all in all the configurations.
%Note that a flash SSD is used in this measurement because ramdisk does not queue but handles all I/O requests synchronously. 

Finally, caching all writes would hurt reliability and performance depending on the characteristics of an NVWC device. For example, caching non-critical writes exacerbates the wear-out of an NVWC device, like flash and PCM, without any gain in application performance. In addition, caching non-critical writes can increase the probability of garbage collection while processing critical writes in flash-based NVWCs. 
%\hj{Similarly, PCM also has the identical issue to be used as an NVWC device due to its limited write endurance caused by its physical properties~\cite{Burr2008,Lee2009,Qureshi2009}.}
%The performance penalty of garbage collections in performance-critical paths are likely to be more severe as the technology trend shifts from single-level cell (SLC) to multi-level cell (MLC)~\cite{Grupp2012} for high density and low price. 

For these reasons, caching only critical writes to NVWC is vital to fully utilize a given NVWC device for application performance. From the analysis based on the realistic workload (Section~\ref{sec:EvaluationPostgreSQL}), we found that all writes do not equally contribute to the application performance. This finding implies that there is a need to classify write I/Os for typical data-intensive applications such as databases and key-value stores.
%To do so, we need to accurately distinguish the criticality of each write. 
