\section{Implementation}\label{sec:Implementation}

We implemented our scheme on x86-64 Linux version 3.12. For the critical process identification, we added a pair of special priority values to dynamically set or clear criticality of a process (or a thread) to the existing \texttt{setpriority()} system call interface. We also added a field to the process descriptor for distinguishing criticality of each process. 

For handling process and I/O dependencies, we implemented criticality inheritance protocols to blocking-based synchronization methods. Specifically, process and I/O criticality inheritances are implemented to the methods that synchronize with a process (e.g., \texttt{mutex\_lock()}) and an ongoing I/O (e.g., \texttt{lock\_buffer()}), respectively. In all the synchronization points, a blocking object is recorded into the descriptor of a process who is about to be blocked for synchronization.

We implemented our admission policy to FlashCache~\cite{FlashCache} version 3.1.1, which is a non-volatile block cache implemented as a kernel module. We modified the admission policy of FlashCache to cache only the writes synchronously requested by both critical and criticality-inherited processes. We also added the support for I/O criticality inheritance to FlashCache. In particular, the modified FlashCache maintains the list of outstanding non-critical writes to disk and searches the list when a critical process requests for reissuing a specific write. If the requested write is found in the list, FlashCache immediately reissues that write to NVWC and discards the result of the original write upon completion.

\begin{figure*}[t]
\centering
\subfigure[Ramdisk NVWC\label{fig:eval-performance-postgres-ramdisk}]{\epsfig{file=../../figures/eval-performance-postgres-ramdisk-capacity.eps, width=3.15in}}
\subfigure[SSD NVWC\label{fig:eval-performance-postgres-ssd}]{\epsfig{file=../../figures/eval-performance-postgres-ssd-capacity.eps, width=3.15in}}
\vspace{-0.1in}
\caption{\textbf{PostgreSQL performance.} \textit{TPC-C throughput is averaged over three runs for each admission policy.}}
\vspace{-0.15in}
\label{fig:eval-performance-postgres}
\end{figure*}

\section{Application Studies}\label{sec:Applications}

To validate the effectiveness of our scheme, we chose two widely deployed applications: PostgreSQL RDBMS~\cite{Postgres} version 9.2 and Redis NoSQL store~\cite{Redis} version 2.8. For the critical process identification, we inserted eleven and two lines of code excluding comments to PostgreSQL and Redis, respectively. This result indicates that adopting the interface for critical process identification is trivial for typical data-intensive applications.

%\subsection{PostgreSQL RDBMS}

%As in other RDBMSes such as MySQL, PostgreSQL uses write-ahead logging (WAL) at runtime and requires scanning all log files to replay committed transactions at recovery time~\cite{Mohan1992}. To mitigate the excessive recovery time while avoiding service disruption, PostgreSQL periodically conducts a variant of fuzzy (i.e., non-quiescent) checkpointing~\cite{Gray1992}, which synchronously writes all dirty buffers to disk in background while allowing concurrent request processing. 

\textbf{PostgreSQL RDBMS.} In PostgreSQL, \textit{backend} is dedicated to client for serving requests while other processes, such as \textit{checkpointer}, \textit{writer}, and \textit{log writer}, carry out I/O jobs in background. The checkpointer flushes all dirty data buffers to disk and writes a special checkpoint record to the log file when the configured number of log files is consumed or the configured timeout happens, whichever comes first. The writer periodically writes some dirty buffers to disk to keep regular backend processes from having to write out dirty buffers. Similarly, the log writer periodically writes out the log buffer to disk in order to reduce the amount of synchronous writes needed for backend processes at commit time.

We classified backends as critical processes by calling the provided interface before starting the main loop of each backend. We also classified a process who is holding \textit{WALWriteLock} as a temporary critical process because WALWriteLock is heavily shared between backends and other processes, and flushing the log buffer to a disk is conducted while holding the lock. This approach is similar to the priority ceiling~\cite{Sha1990} in that a process inherits criticality of a lock when the process acquires the lock. 
%\As exemplified in the PostgreSQL case, our scheme may require additional hints for processes other than request-handling processes when a user-level dependency that cannot be detected at the kernel-level exists in an application.

%\subsection{Redis NoSQL Store}

%Redis extends the memcached model from a cache to a persistent NoSQL object store. In order to provide persistence, Redis has two options: snapshotting and command logging. The snapshotting periodically produces point-in-time snapshots of the dataset. The snapshotting, however, does not provide durability guarantee since up to a few minutes of data can be lost. 
\textbf{Redis NoSQL store.} Redis has two options to provide durability: snapshotting and command logging. The snapshotting periodically produces point-in-time snapshots of the dataset. The snapshotting, however, does not provide complete durability since up to a few minutes of data can be lost. The fully-durable command logging, on the other hand, guarantees the complete durability by synchronously writing an update log to a log file before responding back to the command. In the command logging, log rewriting is periodically conducted to constrain the size of the log file. Though the command logging can provide stronger durability than the snapshotting, it is still advisable to also turn the snapshotting on~\cite{RedisPersistence}. 
%Though the command logging can provide stronger durability than the snapshotting, it is still advisable to also turn the snapshotting on because having a periodic snapshot is beneficial for doing database backups, for faster restarts, and in the event of bugs in the logging engine~\cite{RedisPersistence}. 

Similar to the PostgreSQL case, the snapshotting and log rewriting are conducted by child processes in background while a main server process serves all requests sequentially. Hence, we classified only the main server process as a critical process by calling the provided interface before starting the main event loop.
